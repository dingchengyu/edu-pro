package com.lagou.front.message.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.lagou.common.result.ResponseDTO;
import com.lagou.front.common.UserManager;
import com.lagou.front.message.service.MessageService;
import com.lagou.front.message.vo.request.MessageQueryVo;
import com.lagou.front.message.vo.response.MessageVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/message")
@Api(description = "消息通知相关接口", tags = "消息通知接口")
public class MessageController {

	@Autowired
	private MessageService messageService;
	
    @ApiOperation("查询消息通知列表")
    @PostMapping("/getMessageList")
    public ResponseDTO<Page<MessageVo>> getMessageList(@RequestBody MessageQueryVo param) {
        try{
            Map<String, Object> decode = (Map<String, Object>) ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getDecodedDetails();
            Integer userId = (Integer) decode.get("user_id");
            param.setUserId(userId);
            return ResponseDTO.success(messageService.getMessageList(param));
        }catch (Exception e){
            e.printStackTrace();
            log.info("getMessageList  ","用户未登录！");
        }
        return ResponseDTO.success();
    }
    
    @ApiOperation("更新消息已读")
    @RequestMapping("/updateReadStatus")
    public ResponseDTO<Boolean> updateReadStatus() {
        Map<String, Object> decode = (Map<String, Object>) ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getDecodedDetails();
        Integer userId = (Integer) decode.get("user_id");
    	log.info("getMessageList - updateReadStatus - userId:{}",userId);
        return ResponseDTO.success(messageService.updateReadStatus(userId));
    }
    
    @ApiOperation("获取用户未读消息标识")
    @GetMapping("/getUnReadMessage")
    public ResponseDTO<Boolean> getUnReadMessage(){
        Map<String, Object> decode = (Map<String, Object>) ((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getDecodedDetails();
        Integer userId = (Integer) decode.get("user_id");
        return ResponseDTO.success(messageService.getUnReadMessageFlag(userId));
    }
}
