package com.lagou.front.pay.service;

import com.lagou.front.pay.vo.request.GetPayInfoVo;
import com.lagou.front.pay.vo.request.PayReqVo;
import com.lagou.front.pay.vo.response.OrderVo;
import com.lagou.front.pay.vo.response.PayInfoVo;
import com.lagou.front.pay.vo.response.PayResVo;
import com.lagou.pay.api.dto.CallBackReq;
import com.lagou.pay.api.dto.CallBackRes;

/**
 * @Description:(支付service)   
 * @author: dingchengyu
*/
public interface PayService {
	
	/**
	 * @Description: (创建订单(发起支付))   
	*/
	PayResVo saveOrder(PayReqVo reqVo);
	
	/**
	 * @Description: (支付回调)   
	*/
    CallBackRes callBack(CallBackReq request);
    
    /**
     * @Description: (查询支付结果)   
    */
    OrderVo getPayResult(Integer userId, String orderNo);
    
    /**
     * @Description: (获取支付信息)   
    */
    PayInfoVo getPayInfo(GetPayInfoVo getPayInfoVo);
}
