package com.lagou.front.course.service;

import com.lagou.front.course.model.response.CourseSectionListResult;

public interface SectionService {
    CourseSectionListResult getSectionInfoByCourseId(Integer userId, Integer courseId);
}
