package com.lagou.front.course.service;

import com.lagou.front.course.model.response.CoursePurchasedRecordRespVo;
import com.lagou.front.course.model.response.PurchasedRecordForAppResult;

import java.util.List;

public interface CourseService {
    List<CoursePurchasedRecordRespVo> getAllCoursePurchasedRecord(Integer userId);
}
