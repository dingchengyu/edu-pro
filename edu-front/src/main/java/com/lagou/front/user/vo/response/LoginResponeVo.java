package com.lagou.front.user.vo.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/3/5 15:38
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel("用户登录响应VO")
public class LoginResponeVo implements Serializable {

    @ApiModelProperty(name = "token", value = "token", required = true)
    private String token;
}
