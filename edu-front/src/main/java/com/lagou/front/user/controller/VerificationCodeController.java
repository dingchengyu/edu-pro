package com.lagou.front.user.controller;

import com.lagou.common.result.ResponseDTO;
import com.lagou.user.api.VerificationCodeRemoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 验证码接口
 *
 * @author dingchengyu
 * @date 2022/3/6 16:45
 */
@Slf4j
@RestController
@RequestMapping("/user/vfcode")
public class VerificationCodeController {

    @Autowired
    private VerificationCodeRemoteService verificationCodeRemoteService;

    /**
     * @param telephone
     * @return
     */
    @PostMapping("sendCode")
    public ResponseDTO sendCode(@ApiParam(value = "电话号码", name = "telephone") String telephone) {
        return verificationCodeRemoteService.sendCode(telephone);
    }

    /**
     * 判断验证码是否正确
     *
     * @return
     */
    @PostMapping("checkCode")
    public ResponseDTO checkCode(@ApiParam(value = "电话号码", name = "telephone") String telephone,
                                 @ApiParam(value = "验证码", name = "code") String code) {

        return verificationCodeRemoteService.checkCode(telephone, code);
    }
}
