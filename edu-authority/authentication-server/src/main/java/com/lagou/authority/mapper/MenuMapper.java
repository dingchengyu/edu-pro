package com.lagou.authority.mapper;

import com.lagou.authority.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Repository
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    /**
     * 根据角色id列表关联menu,role_menu_relation表，查询角色下所拥有的菜单
     *
     * @param roleIds
     * @return
     */
    List<Menu> queryByRoleIds(@Param("roleIds") Set<Integer> roleIds);

    /**
     * 查询角色菜单，忽略shown字段值
     *
     * @param roleId
     * @return
     */
    List<Menu> queryByRoleIdIgnoreIsShown(@Param("roleId") Integer roleId);
}
