package com.lagou.authority.controller;


import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.lagou.auth.client.dto.AllocateUserRoleDTO;
import com.lagou.auth.client.dto.RoleDTO;
import com.lagou.auth.client.param.RoleQueryParam;
import com.lagou.authority.entity.Roles;
import com.lagou.authority.service.IRolesService;
import com.lagou.common.entity.vo.Result;
import com.lagou.common.util.ConvertUtils;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Slf4j
@RestController
@RequestMapping("/role")
public class RolesController {
    @Autowired
    private IRolesService roleService;

    @ApiOperation(value = "获取用户角色", notes = "根据用户userId查询用户角色")
    @GetMapping(value = "/getUserRoles")
    public Result<Set<RoleDTO>> getUserRoles(@RequestParam("userId") Integer userId) {
        List<Roles> roles = roleService.queryByUserId(userId);
        Set<RoleDTO> roleSet = Sets.newHashSet();
        if (CollectionUtils.isNotEmpty(roles)) {
            roles.stream().forEach(role -> {
                roleSet.add(ConvertUtils.convert(role, RoleDTO.class));
            });
        }
        return Result.success(roleSet);
    }

    @ApiOperation(value = "创建或更新角色", notes = "创建或更新角色")
    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdateRole(@RequestBody RoleDTO roleDTO) {
        Roles role = ConvertUtils.convert(roleDTO, Roles.class);
        role.setUpdateTime(new Date());
        roleService.saveOrUpdate(role);
        return Result.success();
    }

    @ApiOperation(value = "删除角色", notes = "根据ID删除角色")
    @DeleteMapping(value = "/{id}")
    public Result deleteById(@PathVariable Integer id) {
        return Result.success(roleService.deleteWithAssociation(id));
    }

    @ApiOperation(value = "查询角色", notes = "根据ID查询角色")
    @GetMapping("/{id}")
    public Result<RoleDTO> getById(@PathVariable Integer id) {
        Roles role = roleService.get(id);
        return Result.success(ConvertUtils.convert(role, RoleDTO.class));
    }

    @ApiOperation(value = "查询所有角色", notes = "查询所有角色")
    @GetMapping("/getAll")
    public Result<List<RoleDTO>> getAll() {
        List<Roles> roles = roleService.getAll();
        List<RoleDTO> roleDTOList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(roles)) {
            roles.stream().forEach(role -> {
                roleDTOList.add(ConvertUtils.convert(role, RoleDTO.class));
            });
        }
        return Result.success(roleDTOList);
    }


    @ApiOperation(value = "给用户分配角色", notes = "给用户分配角色，可同时分配多个角色")
    @PostMapping("/allocateUserRoles")
    public Result<Boolean> allocateUserRoles(@RequestBody AllocateUserRoleDTO allocateUserRoleDTO) {
        log.info("Allocate user roles with params:{}", allocateUserRoleDTO);
        roleService.allocateUserRoles(allocateUserRoleDTO);
        return Result.success(Boolean.TRUE);
    }

    @ApiOperation(value = "分布查询角色列表")
    @PostMapping("/getRolePages")
    public Result<Page<RoleDTO>> getRolePages(@RequestBody RoleQueryParam roleQueryParam) {
        Page<Roles> rolePages = roleService.getRolePages(roleQueryParam);
        Page<RoleDTO> roleDTOPages = new Page<>();
        BeanUtils.copyProperties(rolePages, roleDTOPages);
        if (CollectionUtils.isNotEmpty(rolePages.getRecords())) {
            List<RoleDTO> records = rolePages.getRecords().stream()
                    .map(role -> ConvertUtils.convert(role, RoleDTO.class)).collect(Collectors.toList());
            roleDTOPages.setRecords(records);
        }
        return Result.success(roleDTOPages);
    }
}
