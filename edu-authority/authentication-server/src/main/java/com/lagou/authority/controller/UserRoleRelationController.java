package com.lagou.authority.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户和角色关系表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@RestController
@RequestMapping("/authority/user-role-relation")
public class UserRoleRelationController {

}
