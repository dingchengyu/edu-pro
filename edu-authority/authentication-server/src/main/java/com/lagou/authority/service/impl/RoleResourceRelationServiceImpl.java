package com.lagou.authority.service.impl;

import com.lagou.authority.entity.RoleResourceRelation;
import com.lagou.authority.mapper.RoleResourceRelationMapper;
import com.lagou.authority.service.IRoleResourceRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色和资源关系表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Service
public class RoleResourceRelationServiceImpl extends ServiceImpl<RoleResourceRelationMapper, RoleResourceRelation> implements IRoleResourceRelationService {

    @Override
    public boolean removeByRoleId(Integer roleId) {
        return false;
    }

    @Override
    public Set<Integer> queryByRoleId(Integer roleId) {
        return null;
    }

    @Override
    public List<RoleResourceRelation> queryByRoleIds(Set<Integer> roleIds) {
        return null;
    }

    @Override
    public boolean removeByResourceId(Integer resourceId) {
        return false;
    }

    @Override
    public void removeByRoleIdAndResourceIds(Integer roleId, Set<Integer> resourceIds) {

    }
}
