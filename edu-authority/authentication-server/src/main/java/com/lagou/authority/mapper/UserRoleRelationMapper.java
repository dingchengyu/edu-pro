package com.lagou.authority.mapper;

import com.lagou.authority.entity.UserRoleRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和角色关系表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface UserRoleRelationMapper extends BaseMapper<UserRoleRelation> {

}
