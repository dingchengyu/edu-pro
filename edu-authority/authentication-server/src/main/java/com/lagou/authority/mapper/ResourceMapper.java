package com.lagou.authority.mapper;

import com.lagou.authority.entity.Resource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 资源表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Repository
@Mapper
public interface ResourceMapper extends BaseMapper<Resource> {

    /**
     * 根据角色id列表查询关联的资源
     * @param roleIds
     * @return
     */
    List<Resource> queryByRoleIds(@Param("roleIds") Set<Integer> roleIds);

}
