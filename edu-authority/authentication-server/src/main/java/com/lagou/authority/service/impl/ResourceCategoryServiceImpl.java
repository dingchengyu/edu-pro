package com.lagou.authority.service.impl;

import com.lagou.authority.entity.ResourceCategory;
import com.lagou.authority.mapper.ResourceCategoryMapper;
import com.lagou.authority.service.IResourceCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 资源分类表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Service
public class ResourceCategoryServiceImpl extends ServiceImpl<ResourceCategoryMapper, ResourceCategory> implements IResourceCategoryService {

    @Override
    public boolean deleteById(Integer id) {
        return false;
    }
}
