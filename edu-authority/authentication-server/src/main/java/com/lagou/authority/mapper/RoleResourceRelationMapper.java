package com.lagou.authority.mapper;

import com.lagou.authority.entity.RoleResourceRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和资源关系表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface RoleResourceRelationMapper extends BaseMapper<RoleResourceRelation> {

}
