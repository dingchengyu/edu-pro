package com.lagou.authority.mapper;

import com.lagou.authority.entity.Roles;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface RolesMapper extends BaseMapper<Roles> {

}
