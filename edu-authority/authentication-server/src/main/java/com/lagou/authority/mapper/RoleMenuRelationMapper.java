package com.lagou.authority.mapper;

import com.lagou.authority.entity.RoleMenuRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和菜单关系表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface RoleMenuRelationMapper extends BaseMapper<RoleMenuRelation> {

}
