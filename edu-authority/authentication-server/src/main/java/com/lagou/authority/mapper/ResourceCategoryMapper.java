package com.lagou.authority.mapper;

import com.lagou.authority.entity.ResourceCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 资源分类表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface ResourceCategoryMapper extends BaseMapper<ResourceCategory> {

}
