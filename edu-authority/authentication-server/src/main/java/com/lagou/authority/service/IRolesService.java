package com.lagou.authority.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.auth.client.dto.AllocateUserRoleDTO;
import com.lagou.auth.client.param.RoleQueryParam;
import com.lagou.authority.entity.Roles;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface IRolesService extends IService<Roles> {

    /**
     * 获取角色
     *
     * @param id
     * @return
     */
    Roles get(Integer id);

    /**
     * 获取所有角色
     *
     * @return
     */
    List<Roles> getAll();

    /**
     * 根据用户id查询用户拥有的角色
     *
     * @param userId
     * @return
     */
    List<Roles> queryByUserId(Integer userId);


    /**
     * 根据id删除角色
     * 并且关联删除用户-角色，角色-菜单，角色-资源关系
     *
     * @param id
     * @return
     */
    boolean deleteWithAssociation(Integer id);

    /**
     * 给用户分配角色
     *
     * @param allocateUserRoleDTO
     * @return
     */
    void allocateUserRoles(AllocateUserRoleDTO allocateUserRoleDTO);

    Page<Roles> getRolePages(RoleQueryParam roleQueryParam);
}
