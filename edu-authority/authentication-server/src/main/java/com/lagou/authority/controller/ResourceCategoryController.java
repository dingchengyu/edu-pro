package com.lagou.authority.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 资源分类表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@RestController
@RequestMapping("/authority/resource-category")
public class ResourceCategoryController {

}
