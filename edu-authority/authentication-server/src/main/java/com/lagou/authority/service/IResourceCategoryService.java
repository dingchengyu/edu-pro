package com.lagou.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.authority.entity.ResourceCategory;

/**
 * <p>
 * 资源分类表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
public interface IResourceCategoryService extends IService<ResourceCategory> {

    /**
     * 删除资源分类，分类下有资源的不允许删除
     *
     * @param id
     * @return
     */
    boolean deleteById(Integer id);

}
