package com.lagou.authority.service.impl;

import com.lagou.authority.entity.RoleMenuRelation;
import com.lagou.authority.mapper.RoleMenuRelationMapper;
import com.lagou.authority.service.IRoleMenuRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * <p>
 * 角色和菜单关系表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Service
public class RoleMenuRelationServiceImpl extends ServiceImpl<RoleMenuRelationMapper, RoleMenuRelation> implements IRoleMenuRelationService {

    @Override
    public Set<Integer> queryByRoleIds(Set<Integer> roleIds) {
        return null;
    }

    @Override
    public boolean removeByRoleId(Integer roleId) {
        return false;
    }

    @Override
    public boolean removeByMenuId(Integer menuId) {
        return false;
    }

    @Override
    public boolean removeByRoleIdAndMenuIds(Integer roleId, Set<Integer> needToDelMenus) {
        return false;
    }
}
