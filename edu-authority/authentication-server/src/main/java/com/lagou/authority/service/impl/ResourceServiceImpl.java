package com.lagou.authority.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.auth.client.dto.AllocateRoleResourceDTO;
import com.lagou.auth.client.param.ResourceQueryParam;
import com.lagou.authority.entity.Resource;
import com.lagou.authority.mapper.ResourceMapper;
import com.lagou.authority.service.IResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 资源表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-17
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {

    @Override
    public void loadResource() {

    }

    @Override
    public boolean matchRequestUrl(HttpServletRequest authRequest) {
        return false;
    }

    @Override
    public List<Resource> queryByRoleIds(Set<Integer> roleIds) {
        return baseMapper.queryByRoleIds(roleIds);
    }

    @Override
    public boolean deleteWithAssociation(Integer id) {
        return false;
    }

    @Override
    public boolean matchUserResources(Set<Integer> roleIds, HttpServletRequest request) {
        return false;
    }

    @Override
    public Page<Resource> getResourcePages(ResourceQueryParam resourceQueryParam) {
        return null;
    }

    @Override
    public List<Resource> getByCategoryId(Integer categoryId) {
        return null;
    }

    @Override
    public void allocateRoleResources(AllocateRoleResourceDTO allocateRoleResourceDTO) {

    }
}
