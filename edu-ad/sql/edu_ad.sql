/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : edu_ad

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 30/04/2022 16:46:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ad_show_tj
-- ----------------------------
DROP TABLE IF EXISTS `ad_show_tj`;
CREATE TABLE `ad_show_tj`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `f_id` int(0) NULL DEFAULT NULL,
  `f_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_num` int(0) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for operate_log
-- ----------------------------
DROP TABLE IF EXISTS `operate_log`;
CREATE TABLE `operate_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `f_id` int(0) NULL DEFAULT NULL COMMENT '关联id',
  `f_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志类型',
  `operate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作内容',
  `operate_detail` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作详情',
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作者',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for portal
-- ----------------------------
DROP TABLE IF EXISTS `portal`;
CREATE TABLE `portal`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '入口描述',
  `logo` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片url',
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '跳转url',
  `sort` int(0) NOT NULL COMMENT '排序',
  `disable` int(0) NULL DEFAULT 0 COMMENT '是否启用，1 不可用 0 可用 默认0 ',
  `port_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型（WORKPLACE 职场版，SCHOOL 校招版）',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `subsite` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分站',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `operator` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `remark` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '首页入口配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for position_dict_info
-- ----------------------------
DROP TABLE IF EXISTS `position_dict_info`;
CREATE TABLE `position_dict_info`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `position_key` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位关键词',
  `category1` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位一级分类，如：技术、运营、金融、设计等',
  `category2` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '职位二级分类，如：视觉设计、后端开发、DBA、测试',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `operator` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `remark` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '职位词典信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for promote_company
-- ----------------------------
DROP TABLE IF EXISTS `promote_company`;
CREATE TABLE `promote_company`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `company_space_id` int(0) NOT NULL COMMENT 'space ID',
  `company_id` int(0) NOT NULL COMMENT '公司ID',
  `company_short_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司简称',
  `logo` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'logo',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  `position1` int(0) NULL DEFAULT NULL COMMENT '职位id',
  `position2` int(0) NULL DEFAULT NULL COMMENT '职位id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推荐公司信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for promote_company_space
-- ----------------------------
DROP TABLE IF EXISTS `promote_company_space`;
CREATE TABLE `promote_company_space`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '推广位title',
  `ordered` int(0) NULL DEFAULT 0 COMMENT '0为有序，1为随机',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推荐公司信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for promote_hr
-- ----------------------------
DROP TABLE IF EXISTS `promote_hr`;
CREATE TABLE `promote_hr`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `tab_id` int(0) NOT NULL COMMENT 'tab id',
  `hr_id` int(0) NOT NULL COMMENT 'hr ID',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推荐tab对应HR信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for promote_hr_tab
-- ----------------------------
DROP TABLE IF EXISTS `promote_hr_tab`;
CREATE TABLE `promote_hr_tab`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'title',
  `sort` int(0) NOT NULL DEFAULT 0 COMMENT '排序',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '推荐HR tab信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for promotion_ad
-- ----------------------------
DROP TABLE IF EXISTS `promotion_ad`;
CREATE TABLE `promotion_ad`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告名',
  `space_id` int(0) NULL DEFAULT NULL COMMENT '广告位id',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '精确搜索关键词',
  `html_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '静态广告的内容',
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文字一',
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接一',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `status` int(0) NOT NULL DEFAULT 0,
  `priority` int(0) NULL DEFAULT 0 COMMENT '优先级',
  `img` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `promotion_ad_SEG`(`space_id`, `start_time`, `end_time`, `status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1095 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of promotion_ad
-- ----------------------------
INSERT INTO `promotion_ad` VALUES (1074, 'java基础训练营1111', 1, NULL, NULL, 'sdfsadf', 'https://edu.lagou.com/', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 14:28:34', '2020-09-16 15:24:55', 1, 0, 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/17/15949658736951644.jpeg');
INSERT INTO `promotion_ad` VALUES (1075, '精选课程', 2, NULL, NULL, NULL, 'http://edufront.lagou.com/#/content?courseId=1', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 14:28:34', '2020-07-17 13:13:51', 1, 0, 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/17/15949580209796992.png');
INSERT INTO `promotion_ad` VALUES (1076, 'java训练营2', 3, NULL, NULL, NULL, 'http://edufront.lagou.com/#/content?courseId=1', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 14:28:34', '2020-07-17 13:14:11', 1, 0, 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/17/15949583460826312.jpeg');
INSERT INTO `promotion_ad` VALUES (1077, '轮播广告2', 3, NULL, NULL, NULL, 'http://edufront.lagou.com/#/content?courseId=1', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 14:28:34', '2020-07-17 13:07:52', 1, 0, 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/17/15949624525374063.png');
INSERT INTO `promotion_ad` VALUES (1078, '广告3333', 162, NULL, NULL, '这是文本内容111', 'http://www.163.com111', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 14:28:34', '2020-07-17 11:22:31', 1, 2, 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/17/15949561472241579.jpg');
INSERT INTO `promotion_ad` VALUES (1079, '广告', 162, NULL, NULL, '这是文本内容111', 'http://www.163.com111', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 14:30:48', '2020-07-14 14:30:48', 0, 2, 'http://www.baidu.com111');
INSERT INTO `promotion_ad` VALUES (1080, '广告名称111', 169, NULL, NULL, 'text', 'link1', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 16:38:03', '2020-07-14 17:36:34', 0, 0, 'img1');
INSERT INTO `promotion_ad` VALUES (1081, '广告名称111', 169, NULL, NULL, 'text', 'link1', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-14 17:36:50', '2020-07-14 17:36:50', 0, 0, 'img1');
INSERT INTO `promotion_ad` VALUES (1082, '111', NULL, NULL, NULL, NULL, NULL, '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-15 11:57:23', '2020-07-15 11:57:23', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1083, '222', NULL, NULL, NULL, NULL, NULL, '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-15 11:58:00', '2020-07-15 11:58:00', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1084, '123123', NULL, NULL, NULL, NULL, NULL, '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-16 15:52:30', '2020-07-16 15:52:30', 1, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1085, 'storm', NULL, NULL, NULL, NULL, NULL, '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-16 21:48:26', '2020-07-16 21:48:26', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1086, 'stormtest', 3, NULL, NULL, NULL, NULL, '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-16 22:16:48', '2020-07-16 22:16:48', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1087, '撒短发', 3, NULL, NULL, NULL, 'sdfasdfasdfssss', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-16 22:22:47', '2020-07-16 22:22:47', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1088, '冰淇淋套餐', NULL, NULL, NULL, NULL, 'sdfdasdf', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-17 10:07:39', '2020-07-17 11:10:51', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1089, 'dfgfd', 1, NULL, NULL, 'gdfg', 'fdgdg', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-07-30 11:13:32', '2020-07-30 11:13:32', 0, 0, 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/30/15960787995181062.png');
INSERT INTO `promotion_ad` VALUES (1091, '测试广告555', 555, NULL, 'bbbb', 'https://edu.lagou.com/', 'https://edu.lagou.com/', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-08-07 08:05:15', '2020-08-07 21:59:39', 1, 10, 'https://edu.lagou.com/');
INSERT INTO `promotion_ad` VALUES (1092, '测试广告555', 555, NULL, 'bbbb', 'https://edu.lagou.com/', 'https://edu.lagou.com/', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-08-07 21:59:51', '2020-08-07 21:59:51', 1, 10, 'https://edu.lagou.com/');
INSERT INTO `promotion_ad` VALUES (1093, '1sddfdasaf', 2, NULL, NULL, '11', '11', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-08-28 21:10:16', '2020-08-28 21:10:16', 0, 0, NULL);
INSERT INTO `promotion_ad` VALUES (1094, '33', 181, NULL, NULL, NULL, '33', '2022-04-20 17:03:25', '2022-06-29 17:03:25', '2020-09-15 18:18:27', '2020-09-15 18:24:48', 0, 0, NULL);

-- ----------------------------
-- Table structure for promotion_ad_temp
-- ----------------------------
DROP TABLE IF EXISTS `promotion_ad_temp`;
CREATE TABLE `promotion_ad_temp`  (
  `id` int(0) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告名',
  `promotionspace_id` int(0) NULL DEFAULT NULL COMMENT '广告位id',
  `subsite` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分站',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `from_department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `fromMan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `company_id` int(0) NULL DEFAULT NULL COMMENT '公司id',
  `company_short_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司简称',
  `html_content` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文字一',
  `text2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文字二',
  `text3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文字三',
  `img1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片一',
  `img1_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片1小图',
  `img2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片二',
  `img2_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片二小图',
  `img3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片三',
  `img3_small` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片三小图',
  `link1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接一',
  `link2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接二',
  `width` int(0) NULL DEFAULT NULL,
  `height` int(0) NULL DEFAULT NULL,
  `background_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '背景色',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `event_time` datetime(0) NULL DEFAULT NULL COMMENT '事件时间',
  `location` int(0) NULL DEFAULT NULL COMMENT '位置',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NOT NULL DEFAULT 0,
  `position_category1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职位一级分类',
  `position_category2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职位二级分类',
  `pay` int(0) NULL DEFAULT 0 COMMENT '是否付费',
  `contract` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '合同编号',
  `priority` int(0) NULL DEFAULT 0 COMMENT '优先级',
  `ad_type` int(0) NULL DEFAULT 0 COMMENT '类型,1:app端可沟通职位广告',
  `ad_content_type` int(0) NULL DEFAULT 0 COMMENT '广告内容类型，０-图片,1-职位',
  `position_id` int(0) NULL DEFAULT NULL COMMENT '职位ID',
  `summary` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职位概要,一句话',
  `subscript_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角标url',
  `subscript_id` int(0) NULL DEFAULT NULL COMMENT '角标样式id',
  `ad_style_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告样式url',
  `ad_style_id` int(0) NULL DEFAULT NULL COMMENT '广告样式id',
  `is_show_ad_sign` int(0) NULL DEFAULT NULL COMMENT '是否使用广告标识',
  `is_show_logo` int(0) NULL DEFAULT NULL COMMENT '是否展示logo',
  `show_frequency` int(0) NULL DEFAULT NULL COMMENT '展示频次 0展示一次 1展示多次',
  `is_graduates` int(0) NULL DEFAULT NULL COMMENT '0:全部,1:应届生,2:社招',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '白名单用户id，多个用逗号分隔',
  `iswhite` bit(1) NULL DEFAULT b'0' COMMENT '是否支持白名单 0 不支持，1：支持',
  `putinversion` int(0) NULL DEFAULT 0 COMMENT '投放版本：0全部，1企业版，2用户版',
  `filed2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备用字段',
  `filed1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备用字段',
  `img1_small_x` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'iphonex适配图片',
  `img2_small_x` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'iphonex适配图片',
  `img3_small_x` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'iphonex适配图片',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `promotion_ad_SEG`(`promotionspace_id`, `start_time`, `end_time`, `is_del`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for promotion_space
-- ----------------------------
DROP TABLE IF EXISTS `promotion_space`;
CREATE TABLE `promotion_space`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `space_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告位key',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `promotion_space_key_is_del`(`space_key`, `is_del`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 184 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of promotion_space
-- ----------------------------
INSERT INTO `promotion_space` VALUES (1, '首页顶部推荐位1', '666', '2020-07-14 13:03:31', '2020-08-28 16:01:09', 0);
INSERT INTO `promotion_space` VALUES (2, '首页侧边广告位', '888', '2020-07-14 13:03:31', '2020-07-17 11:53:02', 0);
INSERT INTO `promotion_space` VALUES (3, '首页顶部轮播', '999', '2020-07-14 13:03:31', '2020-07-17 13:13:03', 0);
INSERT INTO `promotion_space` VALUES (160, 'sadfa', '123', '2020-07-14 13:03:31', '2020-08-31 18:50:00', 0);
INSERT INTO `promotion_space` VALUES (161, 'ffff', '456', '2020-07-14 13:03:31', '2020-07-17 11:21:26', 0);
INSERT INTO `promotion_space` VALUES (162, '广告名称', '789', '2020-07-14 13:03:31', '2020-07-14 13:03:11', 0);
INSERT INTO `promotion_space` VALUES (163, '广告名称', '78911111', '2020-07-14 13:03:31', '2020-07-14 13:03:11', 0);
INSERT INTO `promotion_space` VALUES (164, '广告名称', '78911111', '2020-07-14 13:03:31', '2020-07-14 13:03:11', 0);
INSERT INTO `promotion_space` VALUES (165, '广告名称', '78911111', '2020-07-14 13:03:31', '2020-07-14 13:03:11', 0);
INSERT INTO `promotion_space` VALUES (166, '广告名称', '78911111', '2020-07-14 13:03:31', '2020-07-14 13:03:11', 0);
INSERT INTO `promotion_space` VALUES (167, '广告名称', '78911111', '2020-07-14 13:03:31', '2020-07-14 13:03:11', 0);
INSERT INTO `promotion_space` VALUES (168, '广告名称111', '33333', '2020-07-14 13:03:31', '2020-07-14 13:03:49', 0);
INSERT INTO `promotion_space` VALUES (169, '名称', 'abcd', '2020-07-14 16:30:38', '2020-07-14 16:31:31', 0);
INSERT INTO `promotion_space` VALUES (170, '冰淇淋套餐', NULL, '2020-07-14 17:39:19', '2020-07-14 17:40:24', 0);
INSERT INTO `promotion_space` VALUES (171, '12111111', NULL, '2020-07-15 12:18:47', '2020-07-15 12:19:06', 0);
INSERT INTO `promotion_space` VALUES (172, '边栏广告', NULL, '2020-08-07 14:32:51', '2020-08-07 14:33:21', 0);
INSERT INTO `promotion_space` VALUES (173, '测试广告位', NULL, NULL, NULL, 0);
INSERT INTO `promotion_space` VALUES (174, '测试广告位11', NULL, NULL, NULL, 0);
INSERT INTO `promotion_space` VALUES (175, '测试广告位3333', '1596804347467', '2020-08-07 07:45:47', '2020-08-07 08:02:08', 0);
INSERT INTO `promotion_space` VALUES (176, 'aabbbaaa', '1596808822338', '2020-08-07 22:00:22', '2020-08-10 15:47:03', 0);
INSERT INTO `promotion_space` VALUES (177, '测试广告位', NULL, '2020-08-28 15:00:28', '2020-08-28 15:00:28', 0);
INSERT INTO `promotion_space` VALUES (178, '测试添加广告位', NULL, '2020-08-28 15:02:41', '2020-08-28 15:02:41', 0);
INSERT INTO `promotion_space` VALUES (179, '测试添加广告位1111', NULL, '2020-08-28 15:07:26', '2020-08-28 15:07:26', 0);
INSERT INTO `promotion_space` VALUES (180, '测试添加广告位名称', NULL, '2020-08-28 15:25:30', '2020-08-28 15:25:30', 0);
INSERT INTO `promotion_space` VALUES (181, '吃1', NULL, '2020-09-15 18:22:51', '2020-09-15 18:22:51', 0);
INSERT INTO `promotion_space` VALUES (182, '111', NULL, '2020-09-28 10:35:50', '2020-09-28 10:35:50', 0);
INSERT INTO `promotion_space` VALUES (183, '123', NULL, '2020-09-28 18:13:00', '2020-09-28 18:13:00', 0);

-- ----------------------------
-- Table structure for search_position
-- ----------------------------
DROP TABLE IF EXISTS `search_position`;
CREATE TABLE `search_position`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关键词',
  `synonyms` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同义词',
  `is_del` bit(1) NULL DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '广告搜索同义词表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for side_b_operate_log
-- ----------------------------
DROP TABLE IF EXISTS `side_b_operate_log`;
CREATE TABLE `side_b_operate_log`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `f_id` int(0) NULL DEFAULT NULL COMMENT '关联id',
  `f_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志类型',
  `operate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operate_detail` varchar(2048) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operator` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `is_del` int(0) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for side_b_recent_news
-- ----------------------------
DROP TABLE IF EXISTS `side_b_recent_news`;
CREATE TABLE `side_b_recent_news`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章链接',
  `online_time` datetime(0) NOT NULL COMMENT '上线时间',
  `top_time` datetime(0) NULL DEFAULT NULL COMMENT '置顶时间',
  `offline_time` datetime(0) NULL DEFAULT NULL COMMENT '下线时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_del` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否删除 1 删除 默认0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for special_ad
-- ----------------------------
DROP TABLE IF EXISTS `special_ad`;
CREATE TABLE `special_ad`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '广告名称',
  `background_img` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专场广告url',
  `description` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `hot_layout` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '2X4' COMMENT '布局',
  `disable` int(0) NULL DEFAULT 0 COMMENT '是否启用，1 不可用 0 可用 默认0 ',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `subsite` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分站',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `operator` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `remark` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `is_del` int(0) NULL DEFAULT 0 COMMENT '是否删除，1 已删除 0 未删除 默认0 ',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '专场广告表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for special_ad_hot
-- ----------------------------
DROP TABLE IF EXISTS `special_ad_hot`;
CREATE TABLE `special_ad_hot`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `special_ad_id` int(0) NOT NULL COMMENT '专场广告ID',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '热点链接',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '专场广告热区信息表' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
