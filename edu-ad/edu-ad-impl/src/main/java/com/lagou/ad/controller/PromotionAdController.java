package com.lagou.ad.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
@RestController
@RequestMapping("/ad/promotion-ad")
public class PromotionAdController {

}
