package com.lagou.ad.service;

import com.lagou.ad.entity.PromotionAd;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
public interface IPromotionAdService extends IService<PromotionAd> {

    List<PromotionAd> getByPromotionSpaceId(Integer spaceId);

}
