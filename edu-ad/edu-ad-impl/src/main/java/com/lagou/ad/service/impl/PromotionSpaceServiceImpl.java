package com.lagou.ad.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.ad.entity.PromotionSpace;
import com.lagou.ad.mapper.PromotionSpaceMapper;
import com.lagou.ad.service.IPromotionSpaceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
@Service
public class PromotionSpaceServiceImpl extends ServiceImpl<PromotionSpaceMapper, PromotionSpace> implements IPromotionSpaceService {

    @Override
    public PromotionSpace getBySpaceKey(String spaceKey) {
        QueryWrapper<PromotionSpace> queryWrapper = new QueryWrapper();
        //根据spaceKey获取PromotionSpace
        queryWrapper.eq("space_key",spaceKey);
        return getBaseMapper().selectOne(queryWrapper);
    }
}
