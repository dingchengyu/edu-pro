package com.lagou.ad;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 *启动类
 * @author dingchengyu
 * @date 2022/1/12 14:13
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.lagou.ad.mapper")
public class AdApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdApplication.class,args);
    }
}
