package com.lagou.ad.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.ad.api.dto.PromotionAdDTO;
import com.lagou.ad.entity.PromotionAd;
import com.lagou.ad.mapper.PromotionAdMapper;
import com.lagou.ad.service.IPromotionAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
@Service
public class PromotionAdServiceImpl extends ServiceImpl<PromotionAdMapper, PromotionAd> implements IPromotionAdService {

    @Override
    public List<PromotionAd> getByPromotionSpaceId(Integer spaceId) {
        QueryWrapper<PromotionAd> queryWrapper = new QueryWrapper<>();
        //根据promoteSpaceId查询SpromoteAd
        queryWrapper.eq("space_id",spaceId);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowString = format.format(new Date());
        //在有效期内
        queryWrapper.gt("end_time",nowString);
        queryWrapper.lt("start_time",nowString);
        queryWrapper.eq("status", PromotionAdDTO.STATUS_ON);
        return getBaseMapper().selectList(queryWrapper);
    }
}
