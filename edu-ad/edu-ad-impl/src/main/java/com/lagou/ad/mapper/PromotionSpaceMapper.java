package com.lagou.ad.mapper;

import com.lagou.ad.entity.PromotionSpace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
public interface PromotionSpaceMapper extends BaseMapper<PromotionSpace> {

}
