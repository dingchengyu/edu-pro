package com.lagou.ad.controller;

import cn.hutool.core.bean.BeanUtil;
import com.lagou.ad.api.AdRemoteService;
import com.lagou.ad.service.IPromotionAdService;
import com.lagou.ad.service.IPromotionSpaceService;
import com.lagou.ad.api.dto.PromotionAdDTO;
import com.lagou.ad.api.dto.PromotionSpaceDTO;
import com.lagou.ad.entity.PromotionAd;
import com.lagou.ad.entity.PromotionSpace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 广告服务接口
 * @author dingchengyu
 * @since 2022-01-12
 */
@RestController
@RequestMapping("/ad")
public class AdController {

    @Autowired
    private IPromotionSpaceService promotionSpaceService;

    @Autowired
    private IPromotionAdService promotionAdService;

    @Autowired
    private AdRemoteService adRemoteService;

    @RequestMapping("/test")
    public List<PromotionAdDTO> test(){
        List<PromotionAdDTO> list = adRemoteService.getAdList();
        return adRemoteService.getAdList();
    }

    /**
     * 查询指定广告定位信息
     * @return
     */
    @RequestMapping("/getOneSpace")
    @ResponseBody
    public PromotionSpace getOneSpace(@RequestParam("id") String id) {
        PromotionSpace space = promotionSpaceService.getById(id);
        return space;
    }

    /**
     * 查询所有广告定位信息列表
     * @return
     */
    @GetMapping("/getAllSpace")
    @ResponseBody
    public String getAllSpace() {
        List<PromotionSpace> promotionSpaces = promotionSpaceService.list();
        return promotionSpaces.toString();
    }

    /**
     * 通过SpaceKey列表查询所有广告信息列表
     * @return
     */
    @RequestMapping("/getAllAd")
    public String getAllPromotionAdsBySpaceKey(@RequestParam("ids") String ids) {
        List<PromotionAd> promotionAds = new ArrayList<>();
        String [] idsArray = ids.split(",");
        for (int i = 0; i < idsArray.length; i++) {
            PromotionAd promotionAd = promotionAdService.getById(Integer.parseInt(idsArray[i]));
            promotionAds.add(promotionAd);
        }
        return promotionAds.toString();
    }


    /**
     * 通过SpaceKey列表获取广告位以及对应的广告信息
     * @return
     */
    @RequestMapping("/getAllAds")
    public List<PromotionSpaceDTO> getAllPromotionAds(@RequestParam("spaceKeys") String[] spaceKeys) {

        List<PromotionSpaceDTO> promotionSpaceDTOList = new ArrayList<>();

        for (String spaceKey:spaceKeys){

            //根据spaceKey获取PromotionSpace
            PromotionSpace promotionSpace = promotionSpaceService.getBySpaceKey(spaceKey);
            //根据PromotionSpaceId获取对应的PromotionAd
            List<PromotionAd> promotionAds = promotionAdService.getByPromotionSpaceId(promotionSpace.getId());


            PromotionSpaceDTO promotionSpaceDTO = new PromotionSpaceDTO();
            List<PromotionAdDTO> promotionAdDTOS = new ArrayList<>(promotionAds.size());

            //拷贝promoteSpace对象的属性到promoteSpaceDTO
            BeanUtil.copyProperties(promotionSpace,promotionSpaceDTO);

            for (PromotionAd promotionAd : promotionAds) {
                PromotionAdDTO promotionAdDTO = new PromotionAdDTO();
                BeanUtil.copyProperties(promotionAd,promotionAdDTO);
                promotionAdDTOS.add(promotionAdDTO);
            }

            promotionSpaceDTO.setAdDTOList(promotionAdDTOS);
            promotionSpaceDTOList.add(promotionSpaceDTO);
        }

        return promotionSpaceDTOList;
    }


}
