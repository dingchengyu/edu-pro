package com.lagou.ad.mapper;

import com.lagou.ad.entity.PromotionAd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
public interface PromotionAdMapper extends BaseMapper<PromotionAd> {

}
