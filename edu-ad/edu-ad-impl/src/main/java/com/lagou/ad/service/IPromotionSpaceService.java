package com.lagou.ad.service;

import com.lagou.ad.entity.PromotionSpace;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-12
 */
public interface IPromotionSpaceService extends IService<PromotionSpace> {

    PromotionSpace getBySpaceKey(String spaceKey);

}
