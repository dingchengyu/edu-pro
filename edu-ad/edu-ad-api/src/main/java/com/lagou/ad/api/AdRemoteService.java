package com.lagou.ad.api;

import com.lagou.ad.api.dto.PromotionAdDTO;
import com.lagou.ad.api.dto.PromotionSpaceDTO;
import com.lagou.common.result.ResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 广告模块暴露的fegin接口（提供给其他服务调用）
 * @author dingchengyu
 * @version 1.0
 * @date 2020/6/18 15:17
 */
@FeignClient(value = "edu-ad",path = "/remote/ad")
public interface AdRemoteService {


    /*
     * 获取所有广告
      注：访问地址、方法名称、请求参数类型、返回参数类型
       需要 与  edu-ad-impl模块中的 AdService.getAdList() 保持完全一致，
       否则fegin接口无法暴露给其他服务使用
       如当前接口方法名为 getAdList  请求路径为 /remote/ad/getAdList  请求参数为空  返回类型为 List<PromotionAdDTO>
       对应 edu-ad-impl  中 AdService暴露出来的接口方法名为 getAdList  请求路径为 /remote/ad/getAdList  请求参数为空  返回类型为 List<PromotionAdDTO>
       如有不同，无法提供fegin接口调用
     */
    @RequestMapping("/getAdList")
    List<PromotionAdDTO> getAdList();

    /**
     * 获取所有广告
     * @param spaceKeys 关键字
     * @return
     */
    @GetMapping("/getAllAds")
    List<PromotionSpaceDTO> getAllAds(@RequestParam("spaceKeys") String[] spaceKeys);


    //广告位的新增或者修改
    @RequestMapping(value = "/space/saveOrUpdate",method = RequestMethod.POST)
    ResponseDTO saveOrUpdate(@RequestBody PromotionSpaceDTO promotionSpaceDTO);

    //获取单个广告位
    @RequestMapping("/space/getSpaceById")
    PromotionSpaceDTO getPromotionSpaceById(@RequestParam("id") Integer id);

    //获取所有广告位
    @RequestMapping("/space/getAllSpaces")
    List<PromotionSpaceDTO> getAllSpaces();


    //新增或者修改广告信息
    @RequestMapping(value = "/saveOrUpdate",method = RequestMethod.POST)
    ResponseDTO saveOrUpdate(@RequestBody PromotionAdDTO promotionAdDTO);


    //根据ID获取对应的广告
    @RequestMapping("/getAdById")
    PromotionAdDTO getAdById(@RequestParam("id") Integer id);

    //获取所有的广告
    @RequestMapping("/updateStatus")
    ResponseDTO updateStatus(@RequestParam("id") Integer id, @RequestParam("status") Integer status);

}
