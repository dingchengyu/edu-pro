package com.lagou.ad.api.dto;

import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class PromotionSpaceDTO {

    private static final long serialVersionUID = 1L;

    private Integer id;


    /**
     * 名称
     */
    private String name;

    /**
     * 广告位key
     */
    private String spaceKey;



    private Date createTime;

    private Date updateTime;

    /**
     * 是否删除，1 已删除 0 未删除 默认0
     */
    private Boolean isDel;


    /**
     *
     */
    private List<PromotionAdDTO> adDTOList;

}
