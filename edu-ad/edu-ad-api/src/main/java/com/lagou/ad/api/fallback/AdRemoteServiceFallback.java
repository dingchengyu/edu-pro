package com.lagou.ad.api.fallback;

import com.lagou.ad.api.AdRemoteService;
import com.lagou.ad.api.dto.PromotionAdDTO;
import com.lagou.ad.api.dto.PromotionSpaceDTO;
import com.lagou.common.result.ResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/5 15:03
 */
@Slf4j
public class AdRemoteServiceFallback implements AdRemoteService {

    @Override
    public List<PromotionAdDTO> getAdList() {
        log.info("AdRemoteService.getAllAds ");
        return null;
    }

    @Override
    public List<PromotionSpaceDTO> getAllAds(String[] spaceKeys) {
        log.info("AdRemoteService.getAllAds,param: {}", StringUtils.join(spaceKeys, ","));
        return null;
    }

    @Override
    public ResponseDTO saveOrUpdate(PromotionSpaceDTO promotionSpaceDTO) {
        log.info("AdRemoteService.saveOrUpdate,param: {} ", promotionSpaceDTO.toString());
        return null;
    }

    @Override
    public PromotionSpaceDTO getPromotionSpaceById(Integer id) {
        log.info("AdRemoteService.getPromotionSpaceById,param id: {}", id);
        return null;
    }

    @Override
    public List<PromotionSpaceDTO> getAllSpaces() {
        log.info("AdRemoteService.getAllSpaces");
        return null;
    }

    @Override
    public ResponseDTO saveOrUpdate(PromotionAdDTO promotionAdDTO) {
        log.info("AdRemoteService.saveOrUpdate,param {}", promotionAdDTO.toString());
        return null;
    }

    @Override
    public PromotionAdDTO getAdById(Integer id) {
        log.info("AdRemoteService.getAdById {}", id);
        return null;
    }

    @Override
    public ResponseDTO updateStatus(Integer id, Integer status) {
        log.info("AdRemoteService.updateStatus,param id: {},status: {}", id, status);
        return null;
    }
}
