package com.lagou.common.jwt;

import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;

import java.util.Map;

/**
 * 解析jwt
 */
@Slf4j
public final class JwtUtil {
    /**
     * 创建JWT
     * @param secret
     * @param claims 创建payload的私有声明（根据特定的业务需要添加，如果要拿这个做验证，一般是需要和jwt的接收方提前沟通好验证方式的）
     * @return
     */
    public static String getAccessToken(String secret, Map<String, Object> claims){

        // 指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
        MacSigner rsaSigner=new MacSigner(secret);
        Jwt jwt = JwtHelper.encode(JSON.toJSONString(claims), rsaSigner);
        return jwt.getEncoded();
    }

    public static Map<String,Object> parseToken(String token){
        Jwt jwt = JwtHelper.decode(token);
        return JSON.parseObject(jwt.getClaims());
    }

    /**
     * 根据传入的token过期时间判断token是否已过期
     * @param expiresIn
     * @return true-已过期，false-没有过期
     */
    public static boolean isExpiresIn(long expiresIn){
        long now=System.currentTimeMillis();
        return now>expiresIn;
    }


    private static final String X_USER_ID = "user_id";
    private static final String X_USER_NAME = "user_name";

    public static JwtResult parse(String signKey, String jwtToken) {
        try {
            Jws<Claims> jwt = Jwts.parser()
                    .setSigningKey(signKey.getBytes())
                    .parseClaimsJws(jwtToken);
            if (null != jwt && null != jwt.getBody()) {
                Integer userId = Integer.parseInt(jwt.getBody().get(X_USER_ID, String.class));
                String userName = jwt.getBody().get(X_USER_NAME, String.class);
                return new JwtResult(userId, userName);
            }
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException e) {
            log.error("user token error :{}", e.getMessage());
        }
        return new JwtResult();
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class JwtResult {
        private Integer userId;
        private String userName;
    }
}
