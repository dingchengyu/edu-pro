package com.lagou.common.mq;

import com.lagou.common.mq.dto.BaseMqDTO;

public interface RocketMqService {

	void convertAndSend(String topic, BaseMqDTO<?> data);
	
	/**
	 * topic 可以参考MQConstant
	 * delayLevel 0 不延时   可以参考 MQConstant.DelayLevel的值
	*/
	void sendDelayed(String topic, BaseMqDTO<?> data, int delayLevel);
}
