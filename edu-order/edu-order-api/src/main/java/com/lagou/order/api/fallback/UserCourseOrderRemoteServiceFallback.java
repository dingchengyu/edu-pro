package com.lagou.order.api.fallback;

import com.lagou.common.result.ResponseDTO;
import com.lagou.order.api.UserCourseOrderRemoteService;
import com.lagou.order.api.dto.CreateShopGoodsOrderReqDTO;
import com.lagou.order.api.dto.UserCourseOrderDTO;
import com.lagou.order.api.dto.UserCourseOrderResDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 14:08
 */
@Component
@Slf4j
public class UserCourseOrderRemoteServiceFallback {
    public ResponseDTO<UserCourseOrderResDTO> saveOrder(CreateShopGoodsOrderReqDTO reqDTO) {
        log.info("UserCourseOrderRemoteService.saveOrder");
        return null;
    }

    public ResponseDTO<UserCourseOrderDTO> getCourseOrderByOrderNo(String orderNo) {
        log.info("UserCourseOrderRemoteService.getCourseOrderByOrderNo");
        return null;
    }

    public ResponseDTO<?> updateOrderStatus(String orderNo, Integer status) {
        log.info("UserCourseOrderRemoteService.updateOrderStatus");
        return null;
    }

    public ResponseDTO<List<UserCourseOrderDTO>> getUserCourseOrderByUserId(Integer userId) {
        log.info("UserCourseOrderRemoteService.getUserCourseOrderByUserId");
        return null;
    }

    public ResponseDTO<Integer> countUserCourseOrderByCoursIds(Integer userId, List<Integer> coursIds) {
        log.info("UserCourseOrderRemoteService.countUserCourseOrderByCoursIds");
        return null;
    }

    public ResponseDTO<Integer> countUserCourseOrderByCourseId(Integer coursId) {
        log.info("UserCourseOrderRemoteService.countUserCourseOrderByCourseId");
        return null;
    }

    public ResponseDTO<List<UserCourseOrderDTO>> getOrderListByCourseId(Integer coursId) {
        log.info("UserCourseOrderRemoteService.getOrderListByCourseId");
        return null;
    }
}
