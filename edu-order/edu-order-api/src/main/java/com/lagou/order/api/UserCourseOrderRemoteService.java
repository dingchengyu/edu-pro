package com.lagou.order.api;

import java.util.List;

import com.lagou.order.api.dto.UserCourseOrderResDTO;
import com.lagou.order.api.fallback.UserCourseOrderRemoteServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.lagou.common.result.ResponseDTO;
import com.lagou.order.api.dto.CreateShopGoodsOrderReqDTO;
import com.lagou.order.api.dto.UserCourseOrderDTO;

/**
 * @Description:(订单服务)   
 * @author: dingchengyu
 */
@FeignClient(name = "edu-order", fallback = UserCourseOrderRemoteServiceFallback.class, path = "/remote/userCourseOrder")
public interface UserCourseOrderRemoteService {

	/**
	 * @Description: (保存支付订单)   
	*/
	@PostMapping("/saveOrder")
	ResponseDTO<UserCourseOrderResDTO> saveOrder(@RequestBody CreateShopGoodsOrderReqDTO reqDTO);
	
	/**
	 * @Description: (根据订单号获取订单信息)   
	*/
	@GetMapping("/getCourseOrderByOrderNo")
	ResponseDTO<UserCourseOrderDTO> getCourseOrderByOrderNo(@RequestParam("orderNo") String orderNo);
	
	/**
	 * @Description: (更新商品订单状态)   
	*/
	@PostMapping("/updateOrderStatus")
	ResponseDTO<?> updateOrderStatus(@RequestParam("orderNo") String orderNo, @RequestParam("status") Integer status);
	
	/**
	 * @Description: (根据用户id查询商品订单)   
	*/
	@GetMapping("/getUserCourseOrderByUserId")
	ResponseDTO<List<UserCourseOrderDTO>> getUserCourseOrderByUserId(@RequestParam("userId") Integer userId);
	
	/**
	 * @Description: (根据用户&课程id统计订单数量)   
	*/
	@GetMapping("/countUserCourseOrderByCoursIds")
	ResponseDTO<Integer> countUserCourseOrderByCoursIds(@RequestParam("userId") Integer userId, @RequestParam("coursIds") List<Integer> coursIds);

	/**
	 * @Description: (根据课程id统计支付成功订单数量)   
	*/
	@GetMapping("/countUserCourseOrderByCourseId")
	ResponseDTO<Integer> countUserCourseOrderByCourseId(@RequestParam("coursId") Integer coursId);
	
	/**
	 * @Description: (根据课程id查询支付成功订单集合)   
	*/
	@GetMapping("/getOrderListByCourseId")
	ResponseDTO<List<UserCourseOrderDTO>> getOrderListByCourseId(@RequestParam("coursId") Integer coursId);
}
