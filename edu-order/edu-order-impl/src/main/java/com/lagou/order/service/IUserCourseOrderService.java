package com.lagou.order.service;

import com.lagou.order.api.dto.CreateShopGoodsOrderReqDTO;
import com.lagou.order.api.dto.UserCourseOrderDTO;
import com.lagou.order.api.dto.UserCourseOrderResDTO;
import com.lagou.order.entity.UserCourseOrder;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户课程订单表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
public interface IUserCourseOrderService extends IService<UserCourseOrder> {

    /**
     * 创建商品订单
     * @param reqDTO
     * @return
     */
    UserCourseOrderResDTO saveOrder(CreateShopGoodsOrderReqDTO reqDTO);

    /**
     * 根据订单号获取订单信息
     * @param orderNo
     * @return
     */
    UserCourseOrderDTO getCourseOrderByOrderNo(String orderNo);

    /**
     * 更新商品订单状态
     * @param orderNo
     * @param status
     */
    void updateOrderStatus(String orderNo,Integer status);

    /**
     * 根据用户信息获取订单列表
     * @param userId
     * @return
     */
    List<UserCourseOrderDTO> getUserCourseOrderByUserId(Integer userId);

    /**
     * 根据用户&课程id统计订单数量
     * @param userId
     * @param coursIds
     * @return
     */
    Integer countUserCourseOrderByCoursIds(Integer userId,List<Integer> coursIds);

    /**
     * 根据课程id统计支付成功订单数量
     * @param coursId
     * @return
     */
    Integer countUserCourseOrderByCourseId(Integer coursId);

    /**
     * 根据课程id查询支付成功订单集合
     * @param coursId
     * @return
     */
    List<UserCourseOrderDTO> getOrderListByCourseId(Integer coursId);
}
