package com.lagou.order.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * AT transaction mode undo table 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
@RestController
@RequestMapping("/order/undo-log")
public class UndoLogController {

}
