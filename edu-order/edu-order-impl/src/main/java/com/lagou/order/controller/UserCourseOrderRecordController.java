package com.lagou.order.controller;


import com.lagou.order.service.IUserCourseOrderRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 课程订单状态日志表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
@RestController
@RequestMapping("/order/record")
public class UserCourseOrderRecordController {

    @Autowired
    IUserCourseOrderRecordService orderRecordService;

    @RequestMapping("/testQueryOrder")
    public List testQueryRecord(){
        return orderRecordService.list();
    }

}
