package com.lagou.order.util;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/21 16:00
 */
public class IDUtil {
    /**
     * 随机id生成，使用雪花算法
     */
    public static Long getRandomId() {
        SnowflakeIdWorker sf = new SnowflakeIdWorker();
        Long id = sf.nextId();
        return id;
    }
}
