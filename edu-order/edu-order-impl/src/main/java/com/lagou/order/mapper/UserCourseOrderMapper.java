package com.lagou.order.mapper;

import com.lagou.order.entity.UserCourseOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户课程订单表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
public interface UserCourseOrderMapper extends BaseMapper<UserCourseOrder> {

}
