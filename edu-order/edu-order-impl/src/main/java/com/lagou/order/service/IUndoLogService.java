package com.lagou.order.service;

import com.lagou.order.entity.UndoLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * AT transaction mode undo table 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
public interface IUndoLogService extends IService<UndoLog> {

}
