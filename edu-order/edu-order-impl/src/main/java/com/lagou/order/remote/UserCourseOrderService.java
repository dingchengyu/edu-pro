package com.lagou.order.remote;

import com.alibaba.fastjson.JSON;
import com.lagou.common.result.ResponseDTO;
import com.lagou.order.api.dto.CreateShopGoodsOrderReqDTO;
import com.lagou.order.api.dto.UserCourseOrderDTO;
import com.lagou.order.api.dto.UserCourseOrderResDTO;
import com.lagou.order.service.IUserCourseOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/25 12:44
 */
@RestController
@RequestMapping("/remote/userCourseOrder")
@Slf4j
public class UserCourseOrderService {

    @Autowired
    private IUserCourseOrderService userCourseOrderService;

    /**
     * 保存支付订单
     * @param reqDTO
     * @return
     */
    @PostMapping("/saveOrder")
    public ResponseDTO<UserCourseOrderResDTO> saveOrder(CreateShopGoodsOrderReqDTO reqDTO) {
        log.info("saveOrder - reqDTO:{}", JSON.toJSONString(reqDTO));
        return ResponseDTO.success(userCourseOrderService.saveOrder(reqDTO));
    }

    /**
     * 根据订单号获取订单信息
     * @param orderNo
     * @return
     */
    @GetMapping("/getCourseOrderByOrderNo")
    public ResponseDTO<UserCourseOrderDTO> getCourseOrderByOrderNo(String orderNo) {
        log.info("getCourseOrderByOrderNo - orderNo:{}",orderNo);
        return ResponseDTO.success(userCourseOrderService.getCourseOrderByOrderNo(orderNo));
    }

    /**
     * 更新商品订单状态
     * @param orderNo
     * @param status
     * @return
     */
    @PostMapping("/updateOrderStatus")
    public ResponseDTO<?> updateOrderStatus(String orderNo, Integer status) {
        log.info("updateOrderStatus - orderNo:{} status:{}",orderNo,status);
        userCourseOrderService.updateOrderStatus(orderNo,status);
        return ResponseDTO.success();
    }

    /**
     * 根据用户id查询商品订单
     * @param userId
     * @return
     */
    @GetMapping("/getUserCourseOrderByUserId")
    public ResponseDTO<List<UserCourseOrderDTO>> getUserCourseOrderByUserId(Integer userId) {
        log.info("getUserCourseOrderByUserId - userId:{}",userId);
        return ResponseDTO.success(userCourseOrderService.getUserCourseOrderByUserId(userId));
    }

    /**
     * 根据用户&课程id统计订单数量
     * @param userId
     * @param coursIds
     * @return
     */
    @GetMapping("/countUserCourseOrderByCoursIds")
    public ResponseDTO<Integer> countUserCourseOrderByCoursIds(Integer userId, List<Integer> coursIds) {
        log.info("countUserCourseOrderByCoursIds - userId:{} coursIds:{}",userId,JSON.toJSONString(coursIds));
        return ResponseDTO.success(userCourseOrderService.countUserCourseOrderByCoursIds(userId, coursIds));
    }

    /**
     * 根据课程id统计支付成功订单数量
     * @param coursId
     * @return
     */
    @GetMapping("/countUserCourseOrderByCourseId")
    public ResponseDTO<Integer> countUserCourseOrderByCourseId(Integer coursId) {
        log.info("countUserCourseOrderByCourseId - coursId:{} ",coursId);
        return ResponseDTO.success(userCourseOrderService.countUserCourseOrderByCourseId(coursId));
    }

    /**
     * 根据课程id查询支付成功订单集合
     * @param coursId
     * @return
     */
    @GetMapping("/getOrderListByCourseId")
    public ResponseDTO<List<UserCourseOrderDTO>> getOrderListByCourseId(Integer coursId) {
        log.info("getOrderListByCourseId - coursId:{} ",coursId);
        return ResponseDTO.success(userCourseOrderService.getOrderListByCourseId(coursId));
    }
}
