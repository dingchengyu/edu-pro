package com.lagou.order.service;

import com.lagou.order.entity.UserCourseOrderRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程订单状态日志表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
public interface IUserCourseOrderRecordService extends IService<UserCourseOrderRecord> {

}
