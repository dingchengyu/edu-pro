package com.lagou.order.mapper;

import com.lagou.order.entity.UserCourseOrderRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程订单状态日志表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
public interface UserCourseOrderRecordMapper extends BaseMapper<UserCourseOrderRecord> {

}
