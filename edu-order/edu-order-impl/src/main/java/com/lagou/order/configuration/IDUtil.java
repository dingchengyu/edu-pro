package com.lagou.order.configuration;

import com.lagou.order.util.SnowflakeIdWorker;

/**
 * id工具类
 * @author dingchengyu
 * @date 2022/2/24 14:36
 */
public class IDUtil {

    /**
     * 随机id生成，使用雪花算法
     */
    public static long getRandomId() {
        SnowflakeIdWorker sf = new SnowflakeIdWorker();
        long id = sf.nextId();
        return id;
    }
}
