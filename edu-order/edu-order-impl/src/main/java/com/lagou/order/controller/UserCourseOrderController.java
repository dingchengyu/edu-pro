package com.lagou.order.controller;


import com.alibaba.fastjson.JSON;
import com.lagou.common.result.ResponseDTO;
import com.lagou.order.api.dto.CreateShopGoodsOrderReqDTO;
import com.lagou.order.api.dto.UserCourseOrderDTO;
import com.lagou.order.api.dto.UserCourseOrderResDTO;
import com.lagou.order.service.IUserCourseOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户课程订单表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class UserCourseOrderController {

    @Autowired
    private IUserCourseOrderService userCourseOrderService;

    @RequestMapping("/testQueryOrder")
    public String testQueryOrder(){
        return userCourseOrderService.list().toString();
    }

    /**
     * 创建订单
     * @param reqDTO
     * @return
     */
    @PostMapping("/saveOrder")
    public ResponseDTO<UserCourseOrderResDTO> saveOrder(@RequestBody CreateShopGoodsOrderReqDTO reqDTO) {
        log.info("saveOrder - reqDTO:{}", JSON.toJSONString(reqDTO));
        return ResponseDTO.success(userCourseOrderService.saveOrder(reqDTO));
    }

    /**
     * 根据订单号获取订单信息
     * @param orderNo
     * @return
     */
    @GetMapping("/getCourseOrderByOrderNo")
    public ResponseDTO<UserCourseOrderDTO> getCourseOrderByOrderNo(@RequestParam("orderNo")String orderNo) {
        log.info("getCourseOrderByOrderNo - orderNo:{}",orderNo);
        return ResponseDTO.success(userCourseOrderService.getCourseOrderByOrderNo(orderNo));
    }

    /**
     * 更新商品订单状态
     * @param orderNo
     * @param status
     * @return
     */
    @PostMapping("/updateOrderStatus")
    public ResponseDTO<?> updateOrderStatus(@RequestParam("orderNo")String orderNo,@RequestParam("status")Integer status) {
        log.info("updateOrderStatus - orderNo:{} status:{}",orderNo,status);
        userCourseOrderService.updateOrderStatus(orderNo,status);
        return ResponseDTO.success();
    }

    /**
     * 根据用户id获取商品订单
     * @param userId
     * @return
     */
    @GetMapping("/getUserCourseOrderByUserId")
    public ResponseDTO<List<UserCourseOrderDTO>> getUserCourseOrderByUserId(@RequestParam("userId") Integer userId){
        log.info("getUserCourseOrderByUserId - userId:{}",userId);
        return ResponseDTO.success(userCourseOrderService.getUserCourseOrderByUserId(userId));
    }

    /**
     * 根据用户&课程id统计订单数量
     * @param userId
     * @param coursIds
     * @return
     */
    @GetMapping("/countUserCourseOrderByCoursIds")
    public ResponseDTO<Integer> countUserCourseOrderByCoursIds(@RequestParam("userId") Integer userId,@RequestParam(
            "coursIds") List<Integer> coursIds){
        log.info("countUserCourseOrderByCoursIds - userId:{} coursIds:{}",userId,JSON.toJSONString(coursIds));
        return ResponseDTO.success(userCourseOrderService.countUserCourseOrderByCoursIds(userId, coursIds));
    }

    /**
     * 根据课程id统计支付成功订单数量
     * @param coursId
     * @return
     */
    @GetMapping("/countUserCourseOrderByCourseId")
    public ResponseDTO<Integer> countUserCourseOrderByCourseId(@RequestParam("coursId") Integer coursId){
        log.info("countUserCourseOrderByCourseId - coursId:{} ",coursId);
        return ResponseDTO.success(userCourseOrderService.countUserCourseOrderByCourseId(coursId));
    }

    /**
     * 根据课程id查询支付成功订单集合
     * @param coursId
     * @return
     */
    @GetMapping("/getOrderListByCourseId")
    public ResponseDTO<List<UserCourseOrderDTO>> getOrderListByCourseId(@RequestParam("coursId") Integer coursId){
        log.info("getOrderListByCourseId - coursId:{} ",coursId);
        return ResponseDTO.success(userCourseOrderService.getOrderListByCourseId(coursId));
    }

}
