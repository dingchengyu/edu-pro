package com.lagou.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.sql.Blob;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * AT transaction mode undo table
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UndoLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * increment id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * branch transaction id
     */
    private Long branchId;

    /**
     * global transaction id
     */
    private String xid;

    /**
     * undo_log context,such as serialization
     */
    private String context;

    /**
     * rollback info
     */
    private Blob rollbackInfo;

    /**
     * 0:normal status,1:defense status
     */
    private Integer logStatus;

    /**
     * create datetime
     */
    private LocalDateTime createTime;

    /**
     * modify datetime
     */
    private LocalDateTime updateTime;


}
