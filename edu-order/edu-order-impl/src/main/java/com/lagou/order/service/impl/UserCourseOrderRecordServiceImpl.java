package com.lagou.order.service.impl;

import com.lagou.order.entity.UserCourseOrderRecord;
import com.lagou.order.mapper.UserCourseOrderRecordMapper;
import com.lagou.order.service.IUserCourseOrderRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程订单状态日志表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
@Service
public class UserCourseOrderRecordServiceImpl extends ServiceImpl<UserCourseOrderRecordMapper, UserCourseOrderRecord> implements IUserCourseOrderRecordService {

}
