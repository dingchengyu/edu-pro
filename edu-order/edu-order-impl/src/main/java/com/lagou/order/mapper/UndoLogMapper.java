package com.lagou.order.mapper;

import com.lagou.order.entity.UndoLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * AT transaction mode undo table Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
public interface UndoLogMapper extends BaseMapper<UndoLog> {

}
