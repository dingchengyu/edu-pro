package com.lagou.order.service.impl;

import com.lagou.order.entity.UndoLog;
import com.lagou.order.mapper.UndoLogMapper;
import com.lagou.order.service.IUndoLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * AT transaction mode undo table 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-19
 */
@Service
public class UndoLogServiceImpl extends ServiceImpl<UndoLogMapper, UndoLog> implements IUndoLogService {

}
