/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : edu_user

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 30/04/2022 16:51:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `parent_id` int(0) NOT NULL COMMENT '父菜单id',
  `href` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `icon` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `order_num` int(0) NULL DEFAULT NULL COMMENT '排序号',
  `shown` tinyint(0) NULL DEFAULT 1 COMMENT '是否展示',
  `level` int(0) NOT NULL COMMENT '菜单层级，从0开始',
  `created_time` datetime(0) NOT NULL COMMENT '创建时间',
  `updated_time` datetime(0) NOT NULL COMMENT '更新时间',
  `created_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '创建人',
  `updated_by` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, -1, 'Menu', 'user', '权限管理', '管理系统角色、菜单、资源', 1, 1, 0, '2020-07-20 15:41:38', '2020-09-29 03:36:18', 'system', '18201288771');
INSERT INTO `menu` VALUES (2, -1, 'Role', 'lock', '角色列表', '管理系统角色', 1, 1, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (3, -1, 'Menu', 'lock', '菜单列表', '管理系统菜单', 2, 1, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (4, -1, 'Resource', 'lock', '资源列表', '管理系统资源', 3, 1, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (5, -1, 'Courses', 'film', '课程管理', '课程的新增、修改、查看、发布、上下架', 2, 1, 0, '2020-07-20 15:41:38', '2020-09-28 11:18:52', 'system', '18201288771');
INSERT INTO `menu` VALUES (6, -1, 'Users', 'user', '用户管理', '用户的查询、禁用、启用', 3, 1, 0, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (7, -1, '', 'setting', '广告管理', '广告、广告位管理', 4, 1, 0, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (8, -1, 'Advertise', 'setting', '广告列表', '广告管理', 1, 1, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (9, -1, 'AdvertiseSpace', 'setting', '广告位列表', '广告位管理', 2, 1, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (10, -1, 'AllocMenu', 'setting', '给角色分配菜单页面', '给角色分配菜单页面路由', 4, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (11, -1, 'AllocResource', 'setting', '给角色分配资源页面', '给角色分配资源页面路由', 5, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (12, -1, 'AddMenu', 'setting', '添加菜单页面', '添加菜单页路由', 6, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (13, -1, 'UpdateMenu', 'setting', '更新菜单页面', '更新菜单页路由', 7, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (14, -1, 'ResourceCategory', 'setting', '资源分类列表页面', '资源分类列表页面路由', 8, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (15, -1, 'AddAdvertise', 'setting', '添加广告页面', '添加广告页面路由', 3, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (16, -1, 'UpdateAdvertise', 'setting', '编辑广告页面', '编辑广告页面路由', 4, 0, 1, '2020-07-20 15:41:38', '2020-09-29 06:11:43', 'system', '18201288771');
INSERT INTO `menu` VALUES (17, -1, 'AddAdvertiseSpace', 'setting', '添加广告位页面', '添加广告位页面路由', 5, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (18, -1, 'UpdateAdvertiseSpace', 'setting', '更新广告位页面', '更新广告位页面路由', 6, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (19, -1, 'CourseItem', 'setting', '课程详情页面', '课程详情页面路由', 1, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (20, -1, 'CourseSections', 'setting', '课时信息页面', '课时信息页面路由', 2, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (21, -1, 'VideoOptions', 'setting', '课时上传视频', '课时上传视频页面路由', 3, 0, 1, '2020-07-20 15:41:38', '2020-07-20 15:41:38', 'system', 'system');
INSERT INTO `menu` VALUES (29, -1, '123', '123', '123', '123', 0, 0, 0, '2020-09-29 04:03:05', '2020-09-29 04:03:05', '18201288771', '18201288771');
INSERT INTO `menu` VALUES (30, -1, '123', '123', '123', '123', 1, 0, 0, '2020-09-29 04:16:12', '2020-09-29 04:16:12', '18201288771', '18201288771');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `portrait` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像地址',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '注册手机',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码（可以为空，支持只用验证码注册、登录）',
  `reg_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册ip',
  `account_non_expired` bit(1) NULL DEFAULT b'1' COMMENT '是否有效用户',
  `credentials_non_expired` bit(1) NULL DEFAULT b'1' COMMENT '账号是否未过期',
  `account_non_locked` bit(1) NULL DEFAULT b'1' COMMENT '是否未锁定',
  `status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'ENABLE' COMMENT '用户状态：ENABLE能登录，DISABLE不能登录',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  `create_time` datetime(0) NOT NULL COMMENT '注册时间',
  `update_time` datetime(0) NOT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_phone_is_del`(`phone`, `is_del`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100030027 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (100030011, '15321919577', 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/06/28/15933251448762927.png', '15321919577', '$2a$10$aobP02xbn3N/v.ZCcXqCI.6cV02rLA.8.nakqyIBLCKPTGPsFvJou', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 10:19:15', '2020-08-14 21:44:25');
INSERT INTO `user` VALUES (100030012, '15510792994', NULL, '15510792994', '$2a$10$h815kSB3UYIct1lPk7tybesRd.JdAPvnJKwSBkauGlNiIJHKpUsuS', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 10:43:01', '2020-07-10 10:43:01');
INSERT INTO `user` VALUES (100030013, '15801363456', NULL, '15801363456', '$2a$10$JsBLd.2kpFjPWx4dGev1Mut..mLLTTXrM2qVb6nCPhOhgFu7RoGmK', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 11:09:11', '2020-07-10 11:09:11');
INSERT INTO `user` VALUES (100030014, '15321919571', NULL, '15321919571', '$2a$10$.15BYpIXvEyUIpcuAEKdN.AcVGWjs7gi4KXTzGMh15aDabkM0LZMe', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 11:10:26', '2020-07-10 11:10:26');
INSERT INTO `user` VALUES (100030015, '15801363195', NULL, '15801363195', '$2a$10$FWWb4sULtpEr72mRfrEWFO5Wxxxfu0gnk8jJvtvmyUgHV7y.AysOm', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 11:21:50', '2020-07-10 11:21:50');
INSERT INTO `user` VALUES (100030016, '15510792995', NULL, '15510792995', '$2a$10$DDOW0oJO9cNm.ZEDNmJmF.AZhsQxoyQ84zSx.UKZBbc58qJWh9HSy', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 11:23:56', '2020-07-10 11:23:56');
INSERT INTO `user` VALUES (100030017, '15811111111', NULL, '15811111111', '$2a$10$j08ZtKUfYeQ5cTCRLoeFeuNDmob1DnLRK7Mfkdhr1/SuUKhqkwkCC', NULL, b'1', b'1', b'1', 'DISABLE', b'1', '2020-07-10 11:25:45', '2020-07-13 10:56:31');
INSERT INTO `user` VALUES (100030018, '15813795039', NULL, '15813795039', '$2a$10$dudhkaLfSJJhpy7q5dqX4uEbmWk9XpS8UcpmzGIM8YI3UWGzOcMHm', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 12:17:35', '2020-07-10 12:17:35');
INSERT INTO `user` VALUES (100030019, '18201288771', 'https://edu-lagou.oss-cn-beijing.aliyuncs.com/images/2020/07/10/15943594999396473.png', '18201288771', '$2a$10$XmknffykNPNMs97wJKtOwem6tR8HGMQTx0PfALGA66311pzrW8rq2', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 12:20:16', '2020-07-10 12:20:16');
INSERT INTO `user` VALUES (100030020, '18211111111', NULL, '18211111111', '$2a$10$zwz0Qp6H0TYZjDs0hKwiU.wKb91ws4xYkfFf1tujgPg/AcPXTChN2', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-10 13:57:41', '2020-07-10 13:57:41');
INSERT INTO `user` VALUES (100030021, '15811111112', NULL, '15811111112', '$2a$10$LeMiCC/TNUXdvSAaXmUmn.WAGcmkcBVKG4oAhbqZDAneCBOUgVc1.', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-13 11:35:20', '2020-07-13 11:35:20');
INSERT INTO `user` VALUES (100030022, '用户8666', NULL, '18201288666', '$2a$10$zO8M7N/f53OAuyo1GqlW5ujlj3KSeb9lKMwNCNVyuLPNtTfKddo2.', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-07-13 17:43:52', '2020-07-13 17:43:52');
INSERT INTO `user` VALUES (100030024, '18631142257', NULL, '18631142257', '$2a$10$DDOW0oJO9cNm.ZEDNmJmF.AZhsQxoyQ84zSx.UKZBbc58qJWh9HSy', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-09-09 15:41:59', '2020-09-09 15:42:03');
INSERT INTO `user` VALUES (100030025, '用户6870', NULL, '17090086870', '$2a$10$.Njlz4JFOQ3QDv5fKFJm..YTb5Ig05U1oUxMS1I2VznnKJOwaDikK', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-09-23 18:34:33', '2020-09-23 18:34:33');
INSERT INTO `user` VALUES (100030026, '18631142258', NULL, '18631142258', '$2a$10$DDOW0oJO9cNm.ZEDNmJmF.AZhsQxoyQ84zSx.UKZBbc58qJWh9HSy', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2020-09-28 15:07:47', '2020-09-28 15:07:51');
INSERT INTO `user` VALUES (100030027, '用户1111', NULL, '15811111111', '$2a$10$2k.JgtBTJKL6s509q8CLK.MwQ7FyBl75MIhTN1.XllnPEHQ/WY1iq', NULL, b'1', b'1', b'1', 'ENABLE', b'0', '2022-04-30 14:22:47', '2022-04-30 14:22:47');

-- ----------------------------
-- Table structure for user_phone_verification_code
-- ----------------------------
DROP TABLE IF EXISTS `user_phone_verification_code`;
CREATE TABLE `user_phone_verification_code`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号',
  `verification_code` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '验证码',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `isCheck` bit(1) NULL DEFAULT b'0' COMMENT '验证码是否校验过',
  `check_times` int(0) NULL DEFAULT 0 COMMENT '校验次数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `l_phone_verification_code_ind_01`(`phone`, `create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33326 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_phone_verification_code
-- ----------------------------
INSERT INTO `user_phone_verification_code` VALUES (33305, '18201288775', '111111', '2020-07-03 23:59:31', b'1', 80);
INSERT INTO `user_phone_verification_code` VALUES (33306, '008615321919577', '472148', '2020-07-17 16:41:21', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33307, '008615321919577', '254709', '2020-07-17 16:53:02', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33308, '0086', '149696', '2020-07-17 17:33:19', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33309, '0086', '164595', '2020-07-17 18:00:48', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33310, '008615321919577', '994241', '2020-07-17 18:01:27', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33311, '008615321919577', '775431', '2020-07-17 18:04:31', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33312, '008615321919577', '610538', '2020-07-17 18:06:45', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33313, '008615321919577', '317040', '2020-07-17 18:17:05', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33314, '008618201288770', '956444', '2020-07-23 16:18:56', b'1', 2);
INSERT INTO `user_phone_verification_code` VALUES (33315, '18201288771', '029570', '2020-07-23 18:01:20', b'1', 2);
INSERT INTO `user_phone_verification_code` VALUES (33316, '18201288771', '058365', '2020-07-23 19:11:35', b'1', 2);
INSERT INTO `user_phone_verification_code` VALUES (33317, '18631142257', '596241', '2020-08-25 13:59:15', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33318, '18631142257', '976339', '2020-08-25 14:08:18', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33319, '008218631142257', '309912', '2020-09-08 21:48:58', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33320, '13580509685', '477501', '2020-09-12 18:03:06', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33321, '17090086870', '430423', '2020-09-23 18:19:42', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33322, '17090086870', '502657', '2020-09-23 18:20:56', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33323, '17090086870', '864045', '2020-09-23 18:22:11', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33324, '15311351040', '239238', '2020-09-23 18:23:46', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33325, '17090086870', '039845', '2020-09-23 18:38:09', b'0', 0);
INSERT INTO `user_phone_verification_code` VALUES (33326, '18358332543', '571197', '2020-09-28 13:39:12', b'0', 0);

-- ----------------------------
-- Table structure for user_weixin
-- ----------------------------
DROP TABLE IF EXISTS `user_weixin`;
CREATE TABLE `user_weixin`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `user_id` int(0) NULL DEFAULT NULL COMMENT '用户id',
  `union_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '认证id,微信对应的时unionId',
  `open_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'openId',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '昵称',
  `portrait` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '城市',
  `sex` int(0) NULL DEFAULT NULL COMMENT '性别, 1-男，2-女',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `oauthId_and_oauthType_unique`(`union_id`, `open_id`, `is_del`) USING BTREE,
  UNIQUE INDEX `userId_and_oauthType_unique_index`(`user_id`, `open_id`, `is_del`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 506561 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_weixin
-- ----------------------------
INSERT INTO `user_weixin` VALUES (506561, 100030019, 'oXEX_svcbl-mCDhWloqlEFNVHgP8', 'oGYgl0u0vZMKVAByQ3hR0i7jpKew', 'leo', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epKy1c3YeeI5vRqSxqDkaYc9XDuPao1BRLFKGf65SiaRIFqHTpeJg90RfrCXCo7WkicpfsPdKTdNTpA/132', '', 1, '2020-07-23 19:12:21', '2020-07-23 19:12:21', b'0');

SET FOREIGN_KEY_CHECKS = 1;
