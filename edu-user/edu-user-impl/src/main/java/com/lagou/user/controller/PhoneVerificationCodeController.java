package com.lagou.user.controller;


import com.lagou.common.result.ResponseDTO;
import com.lagou.user.service.IPhoneVerificationCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
@RestController
@RequestMapping("/user/vfcode")
public class PhoneVerificationCodeController {

    @Autowired
    private IPhoneVerificationCodeService phoneVerificationCodeService;

    /**
     * 发送验证码
     * @return
     */
    @RequestMapping("sendCode")
    public ResponseDTO sendCode(String telephone){
        boolean res = phoneVerificationCodeService.save(telephone);
        if(res == true){
            return ResponseDTO.success();
        }else{
            return ResponseDTO.ofError("");
        }
    }

    /**
     * 判断验证码是否正确
     * @return
     */
    @RequestMapping("checkCode")
    public ResponseDTO checkCode(String telephone,String code){
        ResponseDTO responseDTO = null;
        try {
            boolean checkCode = phoneVerificationCodeService.checkCode(telephone, code);
            responseDTO = ResponseDTO.success(checkCode);
        }catch (Exception e){
            e.printStackTrace();
            responseDTO = ResponseDTO.ofError(e.getMessage());
        }
        return responseDTO;
    }

}
