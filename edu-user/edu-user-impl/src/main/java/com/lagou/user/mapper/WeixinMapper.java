package com.lagou.user.mapper;

import com.lagou.user.entity.Weixin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
public interface WeixinMapper extends BaseMapper<Weixin> {

}
