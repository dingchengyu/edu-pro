package com.lagou.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.common.date.DateUtil;
import com.lagou.common.random.RandomUtil;
import com.lagou.user.entity.PhoneVerificationCode;
import com.lagou.user.exception.ExpireCodeRuntimeException;
import com.lagou.user.exception.IncorrectCodeRuntimteException;
import com.lagou.user.mapper.PhoneVerificationCodeMapper;
import com.lagou.user.service.IPhoneVerificationCodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
@Service
public class PhoneVerificationCodeServiceImpl extends ServiceImpl<PhoneVerificationCodeMapper, PhoneVerificationCode> implements IPhoneVerificationCodeService {

    @Override
    public boolean save(String telephone) {

        //获取动态验证码
        String randomNumber = RandomUtil.getRandomNumber(6);
        //发送验证码
        System.out.println("发送验证码:  "+ randomNumber);

        PhoneVerificationCode phoneVerificationCode = new PhoneVerificationCode();
        phoneVerificationCode.setPhone(telephone);   //设置电话号码
        phoneVerificationCode.setVerificationCode(randomNumber);
        phoneVerificationCode.setCreateTime(new Date());
        phoneVerificationCode.setIsCheck(false);
        phoneVerificationCode.setCheckTimes(0);

        //保存验证码
        boolean res = this.save(phoneVerificationCode);
        return res;
    }

    @Override
    public boolean checkCode(String telephone, String code) {

        //判断验证码是否存在
        QueryWrapper<PhoneVerificationCode> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone",telephone);
        queryWrapper.eq("verification_code",code);
        PhoneVerificationCode phoneVerificationCode = this.getBaseMapper().selectOne(queryWrapper);

        if(phoneVerificationCode == null){
            throw new IncorrectCodeRuntimteException("验证码错误");
        }
        //判断验证码是否过期
        Date now = new Date();
        Date expireDate = DateUtil.rollByMinutes(phoneVerificationCode.getCreateTime(),5);
        if(now.getTime() > expireDate.getTime()){
            throw new ExpireCodeRuntimeException("验证码失效,重新发送");
        }

        phoneVerificationCode.setIsCheck(true);
        int checkTimes = phoneVerificationCode.getCheckTimes()+1;
        phoneVerificationCode.setCheckTimes(checkTimes);

        return updateById(phoneVerificationCode);
    }
}
