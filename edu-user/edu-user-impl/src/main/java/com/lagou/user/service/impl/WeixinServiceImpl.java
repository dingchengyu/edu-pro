package com.lagou.user.service.impl;

import com.lagou.user.entity.Weixin;
import com.lagou.user.mapper.WeixinMapper;
import com.lagou.user.service.IWeixinService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
@Service
public class WeixinServiceImpl extends ServiceImpl<WeixinMapper, Weixin> implements IWeixinService {

}
