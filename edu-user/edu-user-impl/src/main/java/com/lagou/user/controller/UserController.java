package com.lagou.user.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.user.api.UserRemoteService;
import com.lagou.user.api.dto.UserDTO;
import com.lagou.user.api.param.UserQueryParam;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
@Api(tags = "用户接口", produces = "application/json")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRemoteService userRemoteService;

    @GetMapping("/getUserById")
    public UserDTO getUserById(@RequestParam("userId") Integer userId) {
        return userRemoteService.getUserById(userId);
    }

    @GetMapping("/getUserByPhone")
    public UserDTO getUserByPhone(@RequestParam("phone") String phone) {
        return userRemoteService.getUserByPhone(phone);
    }

    @GetMapping("/isRegister")
    public boolean isRegister(@RequestParam("phone") String phone) {
        return userRemoteService.isRegister(phone);
    }

    @GetMapping("/getPagesCourses")
    public Page<UserDTO> getPagesUsers(@RequestParam("currentPage") Integer currentPage,
                                       @RequestParam("pageSize") Integer pageSize) {
        return userRemoteService.getPagesUsers(currentPage, pageSize);
    }

    @PostMapping(value = "/saveUser")
    public UserDTO saveUser(@RequestBody UserDTO userDTO) {
        return userRemoteService.saveUser(userDTO);
    }


    @PostMapping(value = "/updateUser")
    public boolean updateUser(@RequestBody UserDTO userDTO) {
        return userRemoteService.updateUser(userDTO);
    }

    @GetMapping("/isUpdatedPassword")
    public boolean isUpdatedPassword(@RequestParam("userId") Integer userId) {
        return userRemoteService.isUpdatedPassword(userId);
    }

    @PostMapping("/setPassword")
    public boolean setPassword(@RequestParam("userId") Integer userId, @RequestParam("password") String password, @RequestParam("configPassword") String configPassword) {
        return userRemoteService.setPassword(userId, password, configPassword);
    }

    @PostMapping("/updatePassword")
    boolean updatePassword(@RequestParam("userId") Integer userId, @RequestParam("oldPassword") String oldPassword, @RequestParam("newPassword") String newPassword, @RequestParam("configPassword") String configPassword) {
        return userRemoteService.updatePassword(userId, oldPassword, newPassword, configPassword);
    }

    /**
     * 后台查询课程列表的接口
     */
    @PostMapping("/getUserPages")
    public Page<UserDTO> getQueryCourses(@RequestBody UserQueryParam userQueryParam) {
        return userRemoteService.getUserPages(userQueryParam);
    }

    @PostMapping("/forbidUser")
    public boolean forbidUser(@RequestParam Integer userId) {
        return userRemoteService.forbidUser(userId);
    }
}
