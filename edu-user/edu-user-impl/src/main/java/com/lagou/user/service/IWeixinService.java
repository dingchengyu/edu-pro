package com.lagou.user.service;

import com.lagou.user.entity.Weixin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
public interface IWeixinService extends IService<Weixin> {

}
