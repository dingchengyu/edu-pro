package com.lagou.user.controller;


import com.lagou.common.result.ResponseDTO;
import com.lagou.user.api.UserWeixinRemoteService;
import com.lagou.user.api.dto.WeixinDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-18
 */
@RestController
@RequestMapping("/user/weixin")
public class WeixinController {

    @Autowired
    private UserWeixinRemoteService userWeixinRemoteService;

    @GetMapping("/getUserWeixinByUserId")
    public WeixinDTO getUserWeixinByUserId(@RequestParam("userId") Integer userId) {
        return userWeixinRemoteService.getUserWeixinByUserId(userId);
    }

    @GetMapping("/getUserWeixinByOpenId")
    public WeixinDTO getUserWeixinByOpenId(@RequestParam("openId") String openId) {
        return userWeixinRemoteService.getUserWeixinByOpenId(openId);
    }

    @GetMapping("/getUserWeixinByUnionId")
    public WeixinDTO getUserWeixinByUnionId(@RequestParam("unionId") String unionId) {
        return userWeixinRemoteService.getUserWeixinByUnionId(unionId);
    }

    @PostMapping("/saveUserWeixin")
    public WeixinDTO saveUserWeixin(@RequestBody WeixinDTO weixinDTO) {
        return userWeixinRemoteService.saveUserWeixin(weixinDTO);
    }

    @PostMapping("/updateUserWeixin")
    public boolean updateUserWeixin(@RequestBody WeixinDTO weixinDTO) {
        return userWeixinRemoteService.updateUserWeixin(weixinDTO);
    }

    @PostMapping(value = "/bindUserWeixin")
    public ResponseDTO<WeixinDTO> bindUserWeixin(@RequestBody WeixinDTO weixinDTO) {
        return userWeixinRemoteService.bindUserWeixin(weixinDTO);
    }

    @PostMapping("/unBindUserWeixin")
    public boolean unBindUserWeixin(@RequestParam("userId") Integer userId) {
        return userWeixinRemoteService.unBindUserWeixin(userId);
    }

}
