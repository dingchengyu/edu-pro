package com.lagou.user.remote;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.common.date.DateUtil;
import com.lagou.common.result.ResponseDTO;
import com.lagou.user.api.dto.WeixinDTO;
import com.lagou.user.entity.Weixin;
import com.lagou.user.service.IWeixinService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/weixin")
@Slf4j
public class UserWeixinService {

    @Autowired
    private IWeixinService weixinService;

    @GetMapping("/getUserWeixinByUserId")
    public WeixinDTO getUserWeixinByUserId(@RequestParam("userId") Integer userId) {
        LambdaQueryWrapper<Weixin> wrapper = new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getUserId, userId)
                .eq(Weixin::getIsDel, false);
        List<Weixin> weixins = this.weixinService.getBaseMapper().selectList(wrapper);
        if (CollectionUtils.isEmpty(weixins)) {
            return null;
        }
        WeixinDTO dto = new WeixinDTO();
        BeanUtil.copyProperties(weixins.get(0), dto);
        return dto;
    }

    @GetMapping("/getUserWeixinByOpenId")
    public WeixinDTO getUserWeixinByOpenId(@RequestParam("openId") String openId) {
        LambdaQueryWrapper<Weixin> wrapper = new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getOpenId, openId)
                .eq(Weixin::getIsDel, false);
        List<Weixin> weixins = this.weixinService.getBaseMapper().selectList(wrapper);
        if (CollectionUtils.isEmpty(weixins)) {
            return null;
        }
        WeixinDTO dto = new WeixinDTO();
        BeanUtil.copyProperties(weixins.get(0), dto);
        return dto;
    }

    @GetMapping("/getUserWeixinByUnionId")
    public WeixinDTO getUserWeixinByUnionId(@RequestParam("unionId") String unionId) {
        LambdaQueryWrapper<Weixin> wrapper = new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getUnionId, unionId)
                .eq(Weixin::getIsDel, false);
        List<Weixin> weixins = this.weixinService.getBaseMapper().selectList(wrapper);
        if (CollectionUtils.isEmpty(weixins)) {
            return null;
        }
        WeixinDTO dto = new WeixinDTO();
        BeanUtil.copyProperties(weixins.get(0), dto);
        return dto;
    }

    @PostMapping("/saveUserWeixin")
    public WeixinDTO saveUserWeixin(@RequestBody WeixinDTO weixinDTO) {
        Weixin weixin = new Weixin();
        BeanUtil.copyProperties(weixinDTO, weixin);
        weixin.setCreateTime(new Date());
        weixin.setUpdateTime(new Date());
        weixin.setIsDel(false);
        weixin.setId(null);
        boolean result = this.weixinService.save(weixin);
        log.info("微信绑定成功,微信:{}, 结果:{}", weixin, result);
        WeixinDTO dto = new WeixinDTO();
        BeanUtil.copyProperties(weixin, dto);
        return dto;
    }

    @PostMapping("/updateUserWeixin")
    public boolean updateUserWeixin(@RequestBody WeixinDTO weixinDTO) {
        Weixin weixin = new Weixin();
        BeanUtil.copyProperties(weixinDTO, weixin, "id", "create_time");
        weixin.setUpdateTime(new Date());
        weixin.setIsDel(false);
        boolean result = this.weixinService.updateById(weixin);
        log.info("微信绑定成功,微信:{}, 结果:{}", weixin, result);
        return true;
    }

    @PostMapping("/bindUserWeixin")
    public ResponseDTO<WeixinDTO> bindUserWeixin(@RequestBody WeixinDTO weixinDTO) {
        LambdaQueryWrapper<Weixin> wrapper = new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getUserId, weixinDTO.getUserId())
                .eq(Weixin::getUnionId, weixinDTO.getUnionId())
                .eq(Weixin::getIsDel, false);
        List<Weixin> weixins = this.weixinService.getBaseMapper().selectList(wrapper);
        if (CollectionUtils.isNotEmpty(weixins)) {
            log.info("userId:{}, unionId:{}, openId:{} 已绑定，不用处理 ", weixinDTO.getUserId(), weixinDTO.getUnionId(), weixinDTO.getOpenId());
            return ResponseDTO.response(200, "已绑定，无需处理");
        }
        // 该用户已绑定其他unionId
        Weixin userIdWeixin = this.weixinService.getBaseMapper().selectOne(new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getUserId, weixinDTO.getUserId()).eq(Weixin::getIsDel, false).orderByDesc(Weixin::getId));
        if (null != userIdWeixin && !StringUtils.equals(weixinDTO.getUnionId(), userIdWeixin.getUnionId())) {
            log.info("userId:{}, unionId:{}, openId:{} 该用户已绑定其他unionId ", weixinDTO.getUserId(), weixinDTO.getUnionId(), weixinDTO.getOpenId());
            return ResponseDTO.response(201, "该用户已绑定其他unionId");
        }
        // 该unionId已绑定其他userId
        Weixin unionIdWeixin = this.weixinService.getBaseMapper().selectOne(new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getUnionId, weixinDTO.getUnionId()).eq(Weixin::getIsDel, false).orderByDesc(Weixin::getId));
        if (null != unionIdWeixin && null != weixinDTO.getUnionId() && !weixinDTO.getUserId().equals(unionIdWeixin.getUserId())) {
            log.info("userId:{}, unionId:{}, openId:{} 该unionId已绑定其他用户 ", weixinDTO.getUserId(), weixinDTO.getUnionId(), weixinDTO.getOpenId());
            return ResponseDTO.response(202, "该unionId已绑定其他用户");
        }
        // 开始真正绑定
        Weixin weixin = new Weixin();
        BeanUtil.copyProperties(weixinDTO, weixin);
        weixin.setCreateTime(new Date());
        weixin.setUpdateTime(new Date());
        weixin.setIsDel(false);
        weixin.setId(null);
        boolean result = this.weixinService.save(weixin);
        log.info("微信绑定成功,微信:{}, 结果:{}", weixins, result);
        WeixinDTO dto = new WeixinDTO();
        BeanUtil.copyProperties(weixin, dto);
        return ResponseDTO.success(dto);
    }

    @PostMapping("/unBindUserWeixin")
    public boolean unBindUserWeixin(@RequestParam("userId") Integer userId) {
        LambdaQueryWrapper<Weixin> wrapper = new QueryWrapper<Weixin>().lambda()
                .eq(Weixin::getUserId, userId)
                .eq(Weixin::getIsDel, false);
        List<Weixin> weixins = this.weixinService.getBaseMapper().selectList(wrapper);
        if (CollectionUtils.isEmpty(weixins)) {
            return true;
        }
        weixins.forEach(weixin -> {
            weixin.setIsDel(true);
            weixin.setUpdateTime(DateUtil.getNowDate());
            weixinService.updateById(weixin);
            log.info("用户[{}]已解绑微信[{}]", userId, weixin.getOpenId());
        });
        return true;
    }
}
