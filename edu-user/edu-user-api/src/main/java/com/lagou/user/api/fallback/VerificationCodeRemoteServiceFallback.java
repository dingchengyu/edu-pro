package com.lagou.user.api.fallback;

import com.lagou.common.result.ResponseDTO;
import com.lagou.user.api.VerificationCodeRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/7 20:53
 */

@Slf4j
@Component
public class VerificationCodeRemoteServiceFallback implements VerificationCodeRemoteService {
    @Override
    public ResponseDTO sendCode(String telephone) {
        log.info("VerificationCodeRemoteService.sendCode,param telephone: {}" , telephone);
        return null;
    }

    @Override
    public ResponseDTO checkCode(String telephone, String code) {
        log.info("VerificationCodeRemoteService.checkCode,param telephone: {} code:{}",telephone,code);
        return null;
    }
}
