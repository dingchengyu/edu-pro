package com.lagou.user.api.fallback;

import com.lagou.common.result.ResponseDTO;
import com.lagou.user.api.UserWeixinRemoteService;
import com.lagou.user.api.dto.WeixinDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 微信接口异常处理
 *
 * @author dingchengyu
 * @date 2022/2/7 20:16
 */
@Slf4j
public class UserWeixinRemoteServiceFallback implements UserWeixinRemoteService {
    @Override
    public WeixinDTO getUserWeixinByUserId(Integer userId) {
        log.info("UserWeixinRemoteService.getUserWeixinByUserId,param userId: {}", userId);
        return null;
    }

    @Override
    public WeixinDTO getUserWeixinByOpenId(String openId) {
        log.info("UserWeixinRemoteService.getUserWeixinByOpenId,param openId: {}", openId);
        return null;
    }

    @Override
    public WeixinDTO getUserWeixinByUnionId(String unionId) {
        log.info("UserWeixinRemoteService.getUserWeixinByUnionId,param unionId: {}", unionId);
        return null;
    }

    @Override
    public WeixinDTO saveUserWeixin(WeixinDTO weixinDTO) {
        log.info("UserWeixinRemoteService.saveUserWeixin,param weidinDTO: {}", weixinDTO.toString());
        return null;
    }

    @Override
    public boolean updateUserWeixin(WeixinDTO weixinDTO) {
        log.info("UserWeixinRemoteService.updateUserWeixin,param weidinDTO: {}", weixinDTO.toString());
        return false;
    }

    @Override
    public ResponseDTO<WeixinDTO> bindUserWeixin(WeixinDTO weixinDTO) {
        log.info("UserWeixinRemoteService.bindUserWeixin,param weidinDTO: {}", weixinDTO.toString());
        return null;
    }

    @Override
    public boolean unBindUserWeixin(Integer userId) {
        log.info("UserWeixinRemoteService.unBindUserWeixin,param userId: {}", userId);
        return false;
    }
}
