package com.lagou.user.api.fallback;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.user.api.UserRemoteService;
import com.lagou.user.api.dto.UserDTO;
import com.lagou.user.api.param.UserQueryParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
public class UserRemoteServiceFallback implements UserRemoteService {
    @Override
    public UserDTO getUserById(Integer userId) {
        log.info("UserRemoteService.getUserById,param:{}", userId);
        return null;
    }

    @Override
    public UserDTO getUserByPhone(String phone) {
        log.info("UserRemoteService.getUserByPhone,param:{}", phone);
        return null;
    }

    @Override
    public boolean isRegister(String phone) {
        log.info("UserRemoteService.isRegister,param:{}", phone);
        return false;
    }

    @Override
    public Page<UserDTO> getPagesUsers(Integer currentPage, Integer pageSize) {
        log.info("UserRemoteService.getPagesUsers,param:  currentPage:{}  pageSize {}", currentPage, pageSize);
        return null;
    }

    @Override
    public UserDTO saveUser(UserDTO userDTO) {
        log.info("UserRemoteService.saveUser,param:{}", userDTO.toString());
        return null;
    }

    @Override
    public boolean updateUser(UserDTO userDTO) {
        log.info("UserRemoteService.updateUser,param:{}", userDTO.toString());
        return false;
    }

    @Override
    public boolean isUpdatedPassword(Integer userId) {
        log.info("UserRemoteService.isUpdatedPassword,param:{}", userId);
        return false;
    }

    @Override
    public boolean setPassword(Integer userId, String password, String configPassword) {
        log.info("UserRemoteService.setPassword,param: userId:{},password:{},configPassword:{} ", userId, password, configPassword);
        return false;
    }

    @Override
    public boolean updatePassword(Integer userId, String oldPassword, String newPassword, String configPassword) {
        log.info("UserRemoteService.updatePassword,param:  userId:{},oldPassword:{},newPassword:{},configPassword:{}", userId, oldPassword, newPassword, configPassword);
        return false;
    }

    @Override
    public Page<UserDTO> getUserPages(UserQueryParam userQueryParam) {
        log.info("UserRemoteService.getUserPages,param:{}", userQueryParam.toString());
        return null;
    }

    @Override
    public boolean forbidUser(Integer userId) {
        log.info("UserRemoteService.forbidUser,param:{}", userId);
        return false;
    }
}
