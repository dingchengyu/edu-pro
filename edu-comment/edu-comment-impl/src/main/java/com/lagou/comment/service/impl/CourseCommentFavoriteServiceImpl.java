package com.lagou.comment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.comment.entity.CourseComment;
import com.lagou.comment.entity.CourseCommentFavorite;
import com.lagou.comment.mapper.CourseCommentFavoriteMapper;
import com.lagou.comment.service.ICourseCommentFavoriteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 评论点赞表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
@Service
public class CourseCommentFavoriteServiceImpl extends ServiceImpl<CourseCommentFavoriteMapper, CourseCommentFavorite> implements ICourseCommentFavoriteService {


    @Override
    public List<CourseCommentFavorite> getCommentFavoriteRecordList(Integer userId, List<String> commentIds) {
        if (userId == null || CollectionUtils.isEmpty(commentIds)) {
            return null;
        }
        LambdaQueryWrapper wrapper = Wrappers.<CourseCommentFavorite>query().lambda()
                .eq(CourseCommentFavorite::getUserId, userId)
                .in(CourseCommentFavorite::getCommentId, commentIds);
        List<CourseCommentFavorite> favoriteRecords = baseMapper.selectList(wrapper);
        return favoriteRecords;
    }
}

