package com.lagou.comment.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.comment.entity.CourseComment;
import com.lagou.comment.entity.CourseCommentFavorite;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 评论点赞表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
public interface ICourseCommentFavoriteService extends IService<CourseCommentFavorite> {

    List<CourseCommentFavorite> getCommentFavoriteRecordList(Integer userId, List<String> commentIds);
}