package com.lagou.comment.mapper;

import com.lagou.comment.entity.CourseComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
public interface CourseCommentMapper extends BaseMapper<CourseComment> {

}
