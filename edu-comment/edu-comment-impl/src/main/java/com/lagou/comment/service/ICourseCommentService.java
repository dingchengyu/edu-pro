package com.lagou.comment.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.comment.api.dto.CourseCommentDTO;
import com.lagou.comment.entity.CourseComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
public interface ICourseCommentService extends IService<CourseComment> {

    /**
     * 课程评论列表查询
     * @param courseId  课程编号
     * @param pageNum
     * @param pageSize
     * @return
     */
    Page<CourseComment> getCourseCommentList(Integer courseId,  int pageNum, int pageSize);


    /**
     * 保存评论
     * @param courseComment
     * @return
     */
    boolean saveCourseComment(CourseComment courseComment);


    /**
     * 删除评论
     * @param commentId
     * @param userId
     * @return
     */
    boolean updateCommentDelStatusByIdAndUserId(String commentId, Integer userId);




    /**
     * 更新点赞数量 flag true 增加点赞  false去掉点赞
     * @param commentId
     * @param flag
     */
    void updateNumOfLike(String commentId,Boolean flag);

}
