package com.lagou.comment.remote;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lagou.comment.api.dto.CourseCommentFavoriteDTO;
import com.lagou.comment.entity.CourseCommentFavorite;
import com.lagou.comment.service.ICourseCommentFavoriteService;
import com.lagou.comment.service.ICourseCommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/6 19:12
 */
@RestController
@RequestMapping("/remote/comment/favorite")
@Slf4j
public class CourseCommentFavoriteService {

    @Autowired
    ICourseCommentFavoriteService iCourseCommentFavoriteService;

    @Autowired
    ICourseCommentService iCourseCommentService;

    /**
     * 评论点赞
     * @param userId
     * @param commentId
     * @return
     */
    @GetMapping("/favorite")
    public CourseCommentFavoriteDTO favorite(@RequestParam("userId") Integer userId,
                                      @RequestParam("commentId") String commentId){
        CourseCommentFavorite courseCommentFavorite =
                iCourseCommentFavoriteService.getOne(Wrappers.<CourseCommentFavorite>query().lambda()
                .eq(CourseCommentFavorite::getUserId, userId)
                .eq(CourseCommentFavorite::getCommentId, commentId));
        if(courseCommentFavorite == null){
            CourseCommentFavorite favorite = new CourseCommentFavorite();
            favorite.setCommentId(Integer.parseInt(commentId));
            favorite.setUserId(userId);
            iCourseCommentFavoriteService.save(favorite);
            iCourseCommentService.updateNumOfLike(commentId, Boolean.TRUE);
            CourseCommentFavoriteDTO dto = new CourseCommentFavoriteDTO();
            BeanUtil.copyProperties(favorite, dto);
            log.info("用户点赞成功:{}", dto);
            return dto;
        }else {
            return null;
        }

    }

    /**
     * 取消赞
     * @param userId
     * @param commentId
     * @return
     */
    @GetMapping("/cancelFavorite")
    public boolean cancelFavorite(@RequestParam("userId") Integer userId, @RequestParam("commentId") String commentId){
        CourseCommentFavorite courseCommentFavorite =
                iCourseCommentFavoriteService.getOne(Wrappers.<CourseCommentFavorite>query().lambda()
                        .eq(CourseCommentFavorite::getUserId, userId)
                        .eq(CourseCommentFavorite::getCommentId, commentId));
        if(courseCommentFavorite != null){
            courseCommentFavorite.setIsDel(Boolean.FALSE);
            iCourseCommentFavoriteService.updateById(courseCommentFavorite);
            iCourseCommentService.updateNumOfLike(commentId, Boolean.FALSE);
            log.info("用户取消点赞:{}", courseCommentFavorite);
        }
        return Boolean.TRUE;
    }
}
