package com.lagou.comment;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/1/30 20:29
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.lagou")
@MapperScan("com.lagou.comment.mapper")
public class CommentApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommentApplication.class,args);
    }
}
