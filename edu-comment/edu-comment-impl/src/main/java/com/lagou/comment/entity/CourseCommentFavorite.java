package com.lagou.comment.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 评论点赞表
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CourseCommentFavorite implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 用户评论ID
     */
    private Integer commentId;

    /**
     * 是否删除，1 已删除 0 未删除 默认0 
     */
    private Boolean isDel;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
