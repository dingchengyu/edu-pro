package com.lagou.comment.controller;


import com.lagou.comment.api.CourseCommentFavoriteRemoteService;
import com.lagou.comment.api.dto.CourseCommentFavoriteDTO;
import com.lagou.comment.remote.CourseCommentFavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 评论点赞表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
@RestController
@RequestMapping("/commentfavorite/")
public class CourseCommentFavoriteController {

    @Autowired
    private CourseCommentFavoriteRemoteService courseCommentFavoriteService;


    @GetMapping("favorite")
    public CourseCommentFavoriteDTO favorite(@RequestParam("userId") Integer userId,
                                             @RequestParam("commentId") String commentId) {
        return courseCommentFavoriteService.favorite(userId, commentId);
    }

    @GetMapping("cancelFavorite")
    public boolean cancelFavorite(@RequestParam("userId") Integer userId,
                                  @RequestParam("commentId") String commentId) {
        return this.courseCommentFavoriteService.cancelFavorite(userId, commentId);
    }
}
