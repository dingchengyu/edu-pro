package com.lagou.comment.remote;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.comment.api.CourseCommentRemoteService;
import com.lagou.comment.api.dto.CourseCommentDTO;
import com.lagou.comment.api.param.CourseCommentParam;
import com.lagou.comment.entity.CourseComment;
import com.lagou.comment.entity.CourseCommentFavorite;
import com.lagou.comment.service.ICourseCommentFavoriteService;
import com.lagou.comment.service.ICourseCommentService;
import com.lagou.user.api.UserRemoteService;
import com.lagou.user.api.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/3 22:03
 */
@RestController
@RequestMapping("/remote/comment")
@Slf4j
public class CourseCommentService {

    @Autowired
    private ICourseCommentService iCourseCommentService;

    @Autowired
    private ICourseCommentFavoriteService iCourseCommentFavoriteService;

    @Autowired
    private UserRemoteService userRemoteService;

    @PostMapping(value = "/getCourseCommentList",consumes = "application/json")
    public List<CourseCommentDTO> getCourseCommentList(@RequestBody CourseCommentParam courseCommentParam) {
        log.info("获取评论 参数courseCommentParam：{}", JSON.toJSONString(courseCommentParam));
        Integer userId = courseCommentParam.getUserId();
        Integer courseId = courseCommentParam.getCourseId();
        int pageNum = courseCommentParam.getPageNum();
        int pageSize = courseCommentParam.getPageSize();
        Page<CourseComment> courseCommentList = iCourseCommentService.getCourseCommentList(courseId, pageNum, pageSize);
        List<CourseComment> courseComments = courseCommentList.getRecords();
        if (courseComments == null || courseComments.isEmpty()) {
            return Collections.emptyList();
        }

        //获取一级留言的ID集合
        List<String> parentIds = new LinkedList<>();
        for(CourseComment comment:courseComments){
            parentIds.add(comment.getId().toString());
        }

        //批量获取用户点赞的帖子
        Map<String, Boolean> favoriteMapping = getFavoriteCourseCommentMap(userId,parentIds);
        List<CourseCommentDTO> userCommentList = new LinkedList<CourseCommentDTO>();

        //遍历获取用户留言
        for (CourseComment comment : courseComments) {
            CourseCommentDTO commentDTO = new CourseCommentDTO();
            BeanUtils.copyProperties(comment, commentDTO);
            if(favoriteMapping != null && !favoriteMapping.isEmpty()){
                Boolean favoriteTag = favoriteMapping.get(comment.getId());
                commentDTO.setFavoriteTag(favoriteTag == null ? Boolean.FALSE:Boolean.TRUE);
            }
            if(Objects.equals(userId,comment.getUserId())){
                commentDTO.setOwner(Boolean.TRUE);
            }
            UserDTO userDTO = userRemoteService.getUserById(comment.getUserId());
            if(userDTO != null){
                commentDTO.setNickName(userDTO.getName());
            }
            userCommentList.add(commentDTO);
        }
        log.info("返回的评论 userCommentList：{}",JSON.toJSONString(userCommentList));
        return userCommentList;
    }

    @PostMapping(value = "/saveCourseComment",consumes = "application/json")
    public boolean saveCourseComment(CourseCommentDTO comment) {
        if (!checkParams(comment)) {
            log.info("comment 含有为空的字段");
            return false;
        }
        //组装待保存的courseComment
        CourseComment courseComment = new CourseComment();
        courseComment.setCourseId(comment.getCourseId());
        courseComment.setUserId(comment.getUserId());
        courseComment.setComment(comment.getComment());
        courseComment.setLikeCount(0);
        Date nowDate = new Date();
        courseComment.setCreateTime(nowDate);
        courseComment.setUpdateTime(nowDate);
        courseComment.setIsDel(Boolean.FALSE);
        log.info("保存的留言对象={}", JSON.toJSONString(courseComment));
        return iCourseCommentService.saveCourseComment(courseComment);
    }

    @GetMapping("/deleteCourseComment")
    public boolean deleteCourseComment(String commentId, Integer userId) {
        if (commentId == null || userId == null) {
            log.info("参数为空,commentId={},userId={}", commentId, userId);
            return false;
        }
        return iCourseCommentService.updateCommentDelStatusByIdAndUserId(commentId, userId);
    }

    /**
     *
     * @param parentIds
     * @return
     */
    private Map<String, Boolean> getFavoriteCourseCommentMap(Integer userId, List<String> parentIds) {
        if (CollectionUtils.isEmpty(parentIds)) {
            return Collections.emptyMap();
        }
        List<CourseCommentFavorite> favoriteRecords = iCourseCommentFavoriteService.getCommentFavoriteRecordList(userId, parentIds);
        if (CollectionUtils.isEmpty(favoriteRecords)) {
            return Collections.emptyMap();
        }
        Map<String, Boolean> favoriteMapping = new HashMap<>(favoriteRecords.size());
        for (CourseCommentFavorite record : favoriteRecords) {
            favoriteMapping.put(record.getCommentId().toString(), Boolean.TRUE);
        }
        return favoriteMapping;
    }

    /**
     * 检查留言不为空
     * @param comment
     * @return
     */
    private boolean checkParams(CourseCommentDTO comment) {
        if (comment == null) {
            log.info("comment 为 null");
            return false;
        }

        Integer userId = comment.getUserId();
        if (userId == null) {
            log.info("留言中的 userId 为 null");
            return false;
        }

        Integer courseId = comment.getCourseId();
        if (courseId == null) {
            log.info("留言中的 courseId 为 null");
            return false;
        }


        //
        String commentContent = comment.getComment();
        if (StringUtils.isBlank(commentContent)) {
            log.info("留言中的内容 为 null");
            return false;
        }

        return true;
    }
}
