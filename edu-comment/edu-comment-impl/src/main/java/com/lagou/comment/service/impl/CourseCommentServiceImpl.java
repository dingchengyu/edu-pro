package com.lagou.comment.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.comment.entity.CourseComment;
import com.lagou.comment.mapper.CourseCommentMapper;
import com.lagou.comment.service.ICourseCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 评论表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */

@Slf4j
@Service
public class CourseCommentServiceImpl extends ServiceImpl<CourseCommentMapper, CourseComment> implements ICourseCommentService {


    @Override
    public Page<CourseComment> getCourseCommentList(Integer courseId, int pageNum, int pageSize) {
        if (courseId == null) {
            return null;
        }

        if (pageNum < 1) {
            pageNum = 0;
        }
        Page page = new Page(pageNum, pageSize);
        LambdaQueryWrapper wrapper = Wrappers.<CourseComment>query().lambda().eq(CourseComment::getCourseId, courseId);
        return baseMapper.selectPage(page, wrapper);
    }

    @Override
    public boolean saveCourseComment(CourseComment comment) {
        if(comment == null){
            log.error("comment 参数为空");
            return false;
        }
        CourseComment courseComment = new CourseComment();
        courseComment.setCourseId(comment.getCourseId());
        courseComment.setUserId(comment.getUserId());
        courseComment.setComment(comment.getComment());
        courseComment.setLikeCount(0);
        Date nowDate = new Date();
        courseComment.setCreateTime(nowDate);
        courseComment.setUpdateTime(nowDate);
        courseComment.setIsDel(Boolean.FALSE);
//        log.info("保存的留言对象={}", JSON.toJSONString(courseComment));
        int result = baseMapper.insert(courseComment);
        if(result!=0){
            return true;
        }
        return false;
    }

    @Override
    public boolean updateCommentDelStatusByIdAndUserId(String commentId, Integer userId) {
        CourseComment courseComment = new CourseComment();
        courseComment.setId(Integer.parseInt(commentId));
        courseComment.setIsDel(Boolean.FALSE);
        courseComment.setUserId(userId);
        //创建匹配器，组装查询条件
        LambdaQueryWrapper wrapper = Wrappers.<CourseComment>query().lambda()
                .eq(CourseComment::getId, commentId)
                .eq(CourseComment::getUserId, userId);

        //创建实例
        List<CourseComment> courseComments = baseMapper.selectList(wrapper);
        if(CollectionUtils.isEmpty(courseComments)){
            log.error("获取的评论为空 commentId:{} userId:{}",commentId,userId);
            return false;
        }
        CourseComment courseCommentNew = courseComments.get(0);
        courseCommentNew.setIsDel(Boolean.TRUE);

        return Boolean.TRUE;
    }

    @Override
    public void updateNumOfLike(String commentId, Boolean flag) {
        CourseComment courseComment = new CourseComment();
        courseComment.setId(Integer.parseInt(commentId));
        List<CourseComment> courseComments = baseMapper.selectList(Wrappers.<CourseComment>query().lambda()
                .eq(CourseComment::getId, commentId));
        if(CollectionUtils.isEmpty(courseComments)){
            log.error("评论id没有对应的数据 commentId:{}",commentId);
            return;
        }
        CourseComment comment = courseComments.get(0);
        Integer likeCount = comment.getLikeCount();
        if(flag){
            likeCount++;
        }else{
            likeCount--;
        }
        log.info("更新点赞 likeCount：{} flag:{} 以前点赞数量：{}",likeCount ,flag ,comment.getLikeCount());
        comment.setLikeCount(likeCount);
        baseMapper.updateById(comment);
    }
}
