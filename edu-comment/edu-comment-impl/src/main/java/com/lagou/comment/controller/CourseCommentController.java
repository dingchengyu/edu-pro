package com.lagou.comment.controller;


import com.lagou.comment.api.CourseCommentRemoteService;
import com.lagou.comment.api.dto.CourseCommentDTO;
import com.lagou.comment.api.param.CourseCommentParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 评论表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-01-30
 */
@RestController
@RequestMapping("/comment")
public class CourseCommentController {

    @Autowired
    private CourseCommentRemoteService courseCommentRemote;

    @PostMapping(value = "/saveCourseComment",consumes = "application/json")
    boolean saveCourseComment(@RequestBody CourseCommentDTO commentDTO){
        return courseCommentRemote.saveCourseComment(commentDTO);
    }

    /**
     * 获取课程或课时下的用户评论,
     *
     * @return
     */
    @PostMapping("/getCourseCommentList")
    List<CourseCommentDTO> getCourseCommentList(@RequestBody CourseCommentParam courseCommentParam){
        return courseCommentRemote.getCourseCommentList(courseCommentParam);
    }

    /**
     * 逻辑删除课程评论
     *
     * @param commentId
     * @param userId
     * @return
     */
    @GetMapping("/deleteCourseComment")
    boolean deleteCourseComment(@RequestParam("commentId") String commentId,
                                @RequestParam("userId") Integer userId){
        return courseCommentRemote.deleteCourseComment(commentId,userId);
    }

}
