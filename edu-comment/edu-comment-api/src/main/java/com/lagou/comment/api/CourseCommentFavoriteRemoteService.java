package com.lagou.comment.api;

import com.lagou.comment.api.dto.CourseCommentFavoriteDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "edu-comment", contextId = "favorite", path = "/remote/comment/favorite")
public interface CourseCommentFavoriteRemoteService {

    @GetMapping("/favorite")
    CourseCommentFavoriteDTO favorite(@RequestParam("userId") Integer userId,
                                      @RequestParam("commentId") String commentId);

    @GetMapping("/cancelFavorite")
    boolean cancelFavorite(@RequestParam("userId") Integer userId, @RequestParam("commentId") String commentId);

}
