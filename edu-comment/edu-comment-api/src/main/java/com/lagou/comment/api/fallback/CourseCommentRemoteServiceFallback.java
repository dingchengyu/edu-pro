package com.lagou.comment.api.fallback;

import com.lagou.comment.api.CourseCommentRemoteService;
import com.lagou.comment.api.dto.CourseCommentDTO;
import com.lagou.comment.api.param.CourseCommentParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/6 16:28
 */
@Slf4j
public class CourseCommentRemoteServiceFallback implements CourseCommentRemoteService {
    @Override
    public List<CourseCommentDTO> getCourseCommentList(CourseCommentParam courseCommentParam) {
        log.info("CourseCommentRemoteService.getCourseCommentList,param {}", courseCommentParam.toString());
        return null;
    }

    @Override
    public boolean saveCourseComment(CourseCommentDTO comment) {
        log.info("CourseCommentRemoteService.saveCourseComment,param {}", comment.toString());
        return false;
    }

    @Override
    public boolean deleteCourseComment(String commentId, Integer userId) {
        log.info("CourseCommentRemoteService.deleteCourseComment,param commentId：{}, userId: {}", commentId, userId);
        return false;
    }
}
