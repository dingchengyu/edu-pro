package com.lagou.comment.api.fallback;

import com.lagou.comment.api.CourseCommentFavoriteRemoteService;
import com.lagou.comment.api.dto.CourseCommentFavoriteDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/6 19:05
 */
@Slf4j
public class CourseCommentFavoriteRemoteServiceFallback implements CourseCommentFavoriteRemoteService {

    @Override
    public CourseCommentFavoriteDTO favorite(Integer userId, String commentId) {
        log.info("CourseCommentFavoriteRemoteService.favorite,param userId: {} , commentId: {}", userId, commentId);
        return null;
    }

    @Override
    public boolean cancelFavorite(Integer userId, String commentId) {
        log.info("CourseCommentFavoriteRemoteService.cancelFavorite,param userId: {}, commentId: {}",
                userId, commentId);
        return false;
    }
}
