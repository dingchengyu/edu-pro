package com.lagou.comment.api;


import com.lagou.comment.api.dto.CourseCommentDTO;
import com.lagou.comment.api.param.CourseCommentParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

@FeignClient(name = "edu-comment", contextId = "comment",path = "/remote/comment")
public interface CourseCommentRemoteService {
    /**
     * 获取课程或课时下的用户评论,
     * @return
     */
    @PostMapping(value = "/getCourseCommentList",consumes = "application/json")
    List<CourseCommentDTO> getCourseCommentList(@RequestBody CourseCommentParam courseCommentParam);

    /**
     * 保存课程评论
     *
     * @param comment
     * @return
     */
    @PostMapping(value = "/saveCourseComment",consumes = "application/json")
    boolean saveCourseComment(@RequestBody CourseCommentDTO comment);
    /**
     * 逻辑删除课程评论
     *
     * @param commentId
     * @param userId
     * @return
     */
    @GetMapping("/deleteCourseComment")
    boolean deleteCourseComment(@RequestParam("commentId") String commentId, @RequestParam("userId") Integer userId);

}
