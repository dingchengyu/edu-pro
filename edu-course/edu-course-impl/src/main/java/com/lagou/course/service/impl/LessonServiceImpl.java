package com.lagou.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.course.api.enums.LessonStatus;
import com.lagou.course.entity.Lesson;
import com.lagou.course.mapper.LessonMapper;
import com.lagou.course.service.ILessonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lagou.course.service.IMediaService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * 课程节内容 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
@Service
public class LessonServiceImpl extends ServiceImpl<LessonMapper, Lesson> implements ILessonService {

    @Autowired
    private IMediaService mediaService;

    @Override
    public Integer getReleaseCourse(Integer courseId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("course_id",courseId);

        queryWrapper.eq("status", LessonStatus.RELEASE.getCode());
        queryWrapper.eq("is_del",Boolean.FALSE);
        return baseMapper.selectCount(queryWrapper);
    }

    @Override
    public List<Lesson> getBySectionId(Integer sectionId) {
        QueryWrapper<Lesson> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("section_id",sectionId);
        queryWrapper.eq("is_del",Boolean.FALSE);
        queryWrapper.orderByAsc("order_num");
        List<Lesson> lessons = baseMapper.selectList(queryWrapper);
        if(CollectionUtils.isEmpty(lessons)){
            return Collections.emptyList();
        }
        return lessons;
    }
}
