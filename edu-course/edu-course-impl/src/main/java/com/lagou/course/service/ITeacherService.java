package com.lagou.course.service;

import com.lagou.course.api.dto.TeacherDTO;
import com.lagou.course.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface ITeacherService extends IService<Teacher> {

    boolean saveOrUpdate(TeacherDTO entity);

}
