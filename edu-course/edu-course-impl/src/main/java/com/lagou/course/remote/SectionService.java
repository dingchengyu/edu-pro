package com.lagou.course.remote;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.common.util.ValidateUtils;
import com.lagou.course.api.SectionRemoteService;
import com.lagou.course.api.dto.LessonDTO;
import com.lagou.course.api.dto.MediaDTO;
import com.lagou.course.api.dto.SectionDTO;
import com.lagou.course.api.enums.LessonStatus;
import com.lagou.course.api.enums.SectionStatus;
import com.lagou.course.entity.Lesson;
import com.lagou.course.entity.Media;
import com.lagou.course.entity.Section;
import com.lagou.course.service.ILessonService;
import com.lagou.course.service.IMediaService;
import com.lagou.course.service.ISectionService;
import com.lagou.message.api.MessageRemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Date:     2020/6/21 10:47
 * Description:课程章节操作
 */
@Slf4j
@RestController
@RequestMapping("/remote/section")
public class SectionService {
    @Autowired
    private ISectionService sectionService;
    @Autowired
    private ILessonService lessonService;
    @Autowired
    private MediaService mediaService;

    @PostMapping(value = "/saveOrUpdateSection", consumes = "application/json")
    public boolean saveOrUpdateSection(@RequestBody SectionDTO sectionDTO) {
        Section section = new Section();
        BeanUtils.copyProperties(sectionDTO, section);
        if (sectionDTO.getId() == null) {
            section.setCreateTime(new Date());
        }
        section.setUpdateTime(new Date());
        log.info("保存更新章节信息：{}", JSON.toJSONString(section));
        return sectionService.saveOrUpdate(section);
    }

    @GetMapping(value = "/getSectionAndLesson")
    public List<SectionDTO> getSectionAndLesson(@RequestParam Integer courseId) {
        QueryWrapper<Section> query = new QueryWrapper<>();
        query.eq("course_id", courseId);
        query.orderByAsc("order_num");
        List<Section> sections = sectionService.list(query);
        if (CollectionUtils.isEmpty(sections)) {
            log.info("获取章节信息为空直接返回");
            return Collections.emptyList();
        }
        List<SectionDTO> sectionDTOS = sections.stream().map(section -> {
            SectionDTO sectionDTO = new SectionDTO();
            BeanUtils.copyProperties(section, sectionDTO);
            Integer sectionId = section.getId();
            List<Lesson> lessons = lessonService.getBySectionId(sectionId);
            if (CollectionUtils.isEmpty(lessons)) {
                return sectionDTO;
            }
            List<LessonDTO> lessonDTOs = lessons.stream().map(lesson -> {
                LessonDTO lessonDTO = new LessonDTO();
                BeanUtil.copyProperties(lesson, lessonDTO);
                Integer lessonId = lesson.getId();
                MediaDTO mediaDTO = mediaService.getByLessonId(lessonId);
                if (mediaDTO != null) {
                    lessonDTO.setDurationNum(mediaDTO.getDurationNum());
                    lessonDTO.setDuration(mediaDTO.getDuration());
                }

                return lessonDTO;
            }).collect(Collectors.toList());
            sectionDTO.setLessonDTOS(lessonDTOs);
            return sectionDTO;
        }).collect(Collectors.toList());


        return sectionDTOS;
    }

    @GetMapping(value = "/getBySectionId", consumes = "application/json")
    public SectionDTO getBySectionId(@RequestParam Integer sectionId) {
        Section section = sectionService.getById(sectionId);
        if (section == null) {
            return null;
        }
        SectionDTO sectionDTO = new SectionDTO();
        BeanUtils.copyProperties(section, sectionDTO);
        return sectionDTO;
    }
}
