package com.lagou.course.mapper;

import com.lagou.course.entity.PlayHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface PlayHistoryMapper extends BaseMapper<PlayHistory> {

}
