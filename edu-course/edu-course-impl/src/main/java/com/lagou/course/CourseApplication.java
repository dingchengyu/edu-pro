package com.lagou.course;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 *启动类
 * @author dingchengyu
 * @date 2022/1/12 14:13
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.lagou")
@MapperScan("com.lagou.course.mapper")
public class CourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseApplication.class,args);
    }
}
