package com.lagou.course.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lagou.course.api.dto.TeacherDTO;
import com.lagou.course.entity.Teacher;
import com.lagou.course.mapper.TeacherMapper;
import com.lagou.course.service.ITeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements ITeacherService {

    @Override
    public boolean saveOrUpdate(TeacherDTO entity) {
        Teacher teacher = getOne(Wrappers.<Teacher>query().lambda()
        .eq(Teacher::getCourseId,entity.getCourseId()));
        if(Objects.isNull(teacher)){
            teacher = new Teacher();
            teacher.setCreateTime(new Date());
        }
        teacher.setCourseId(entity.getCourseId());
        teacher.setTeacherName(entity.getTeacherName());
        teacher.setPosition(entity.getPosition());
        teacher.setDescription(entity.getDescription());
        teacher.setUpdateTime(new Date());
        return this.saveOrUpdate(teacher);
    }
}
