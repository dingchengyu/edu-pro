package com.lagou.course.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.course.entity.Course;
import com.lagou.course.mapper.CourseMapper;
import com.lagou.course.service.ICourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {

    @Override
    public IPage<Course> selectPage(Page page, Wrapper wrapper) {
        return baseMapper.selectPage(page, wrapper);
    }

    @Override
    @Transactional
    public void courseAutoOnline() {
        UpdateWrapper<Course> updateWrapper = new UpdateWrapper<>();
        updateWrapper.set("status", 1).set("update_time", new Date());
        updateWrapper.eq("status",0).isNotNull("auto_online_time")
                .le("auto_online_time", new Date());
        update(updateWrapper);
    }
}
