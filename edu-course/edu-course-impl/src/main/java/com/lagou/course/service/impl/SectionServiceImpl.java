package com.lagou.course.service.impl;

import com.lagou.course.entity.Section;
import com.lagou.course.mapper.SectionMapper;
import com.lagou.course.service.ISectionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
@Service
public class SectionServiceImpl extends ServiceImpl<SectionMapper, Section> implements ISectionService {

}
