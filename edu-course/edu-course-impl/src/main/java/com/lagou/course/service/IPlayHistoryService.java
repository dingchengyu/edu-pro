package com.lagou.course.service;

import com.lagou.course.api.dto.PlayHistoryDTO;
import com.lagou.course.entity.PlayHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface IPlayHistoryService extends IService<PlayHistory> {

    PlayHistory getByUserIdAndCourseId(Integer userId, Integer courseId);

    void saveCourseHistoryNode(PlayHistoryDTO playHistoryDTO);
}
