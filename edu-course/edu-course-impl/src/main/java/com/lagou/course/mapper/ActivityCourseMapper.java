package com.lagou.course.mapper;

import com.lagou.course.entity.ActivityCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 活动课程表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface ActivityCourseMapper extends BaseMapper<ActivityCourse> {

    int updateStock(@Param("id") Integer id, @Param("num") Integer num);

}
