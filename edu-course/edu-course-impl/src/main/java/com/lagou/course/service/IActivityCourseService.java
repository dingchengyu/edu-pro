package com.lagou.course.service;

import com.lagou.course.api.dto.ActivityCourseDTO;
import com.lagou.course.entity.ActivityCourse;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 活动课程表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
public interface IActivityCourseService extends IService<ActivityCourse> {

    void saveActivityCourse(ActivityCourseDTO reqDTO);

    void saveOrUpdateActivityCourse(ActivityCourseDTO reqDTO);

    boolean updateActivityCourseStatus(ActivityCourseDTO reqDTO);

    ActivityCourseDTO getByCourseId(Integer courseId);

    void updateActivityCourseStock(Integer courseId,String orderNo);
}
