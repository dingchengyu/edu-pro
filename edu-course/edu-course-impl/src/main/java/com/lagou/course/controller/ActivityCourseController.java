package com.lagou.course.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 活动课程表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-17
 */
@RestController
@RequestMapping("/course/activity-course")
public class ActivityCourseController {

}
