package com.lagou.course.remote;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.course.api.dto.PlayHistoryDTO;
import com.lagou.course.entity.PlayHistory;
import com.lagou.course.service.IPlayHistoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Author:   dingchengyu
 * Date:     2020/6/19 20:24
 * Description: 播放历史
 */
@Service
public class PlayHistoryService  {
    @Autowired
    private IPlayHistoryService playHistoryService;

    public void saveCourseHistoryNode(PlayHistoryDTO playHistoryDTO) {
        playHistoryService.saveCourseHistoryNode(playHistoryDTO);
    }

    public List<Integer> hasStudyLessons(Integer lagouUserId, Integer courseId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id",lagouUserId);
        queryWrapper.eq("course_id",courseId);
        queryWrapper.eq("is_del",Boolean.FALSE);
        List<PlayHistory> coursePlayHistorys = playHistoryService.list(queryWrapper);
        if(CollectionUtils.isEmpty(coursePlayHistorys)){
            return Collections.emptyList();
        }
        return coursePlayHistorys.stream()
                .map(PlayHistory::getLessonId)
                .collect(Collectors.toList());
    }

    public PlayHistoryDTO getByLessonId(Integer lessonId,Integer userId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("lesson_id",lessonId);
        queryWrapper.eq("user_id",userId);
        queryWrapper.eq("is_del",Boolean.FALSE);
        List<PlayHistory> courserPlayHistorys = playHistoryService.list(queryWrapper);
        if(CollectionUtils.isEmpty(courserPlayHistorys)){
            return null;
        }
        PlayHistory coursePlayHistory = courserPlayHistorys.get(0);
        PlayHistoryDTO coursePlayHistoryDTO = new PlayHistoryDTO();
        BeanUtils.copyProperties(coursePlayHistory,coursePlayHistoryDTO);
        return coursePlayHistoryDTO;
    }

    public PlayHistoryDTO getRecordLearn(Integer courseId,Integer userId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id",courseId);
        queryWrapper.eq("user_id",userId);
        queryWrapper.eq("is_del",Boolean.FALSE);
        queryWrapper.orderByDesc("create_time");
        List<PlayHistory> courserPlayHistorys = playHistoryService.list(queryWrapper);
        if(CollectionUtils.isEmpty(courserPlayHistorys)){
            return null;
        }
        PlayHistory coursePlayHistory = courserPlayHistorys.get(0);
        PlayHistoryDTO coursePlayHistoryDTO = new PlayHistoryDTO();
        BeanUtils.copyProperties(coursePlayHistory,coursePlayHistoryDTO);
        return coursePlayHistoryDTO;
    }
   public PlayHistoryDTO getByUserIdAndCourseId(Integer userId, Integer courseId){
       PlayHistory coursePlayHistory = playHistoryService.getByUserIdAndCourseId(userId, courseId);
       if(coursePlayHistory == null){
        return  null;
       }
       PlayHistoryDTO coursePlayHistoryDTO = new PlayHistoryDTO();
       BeanUtils.copyProperties(coursePlayHistory,coursePlayHistoryDTO);
       return coursePlayHistoryDTO;
   }
}
