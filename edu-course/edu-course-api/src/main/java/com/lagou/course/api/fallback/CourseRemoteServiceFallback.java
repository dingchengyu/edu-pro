package com.lagou.course.api.fallback;

import com.lagou.course.api.CourseRemoteService;
import com.lagou.course.api.dto.CourseDTO;
import com.lagou.course.api.dto.PageResultDTO;
import com.lagou.course.api.param.CourseQueryParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/25 14:07
 */
@Slf4j
public class CourseRemoteServiceFallback implements CourseRemoteService {
    @Override
    public List<CourseDTO> getAllCourses(Integer userId) {
        return null;
    }

    @Override
    public List<CourseDTO> getPurchasedCourse(Integer userId) {
        return null;
    }

    @Override
    public CourseDTO getUserCourseById(Integer courseId, Integer userId) {
        return null;
    }

    @Override
    public CourseDTO getCourseById(Integer courseId) {
        return null;
    }

    @Override
    public boolean saveOrUpdateCourse(CourseDTO courseDTO) {
        return false;
    }

    @Override
    public PageResultDTO<CourseDTO> getQueryCourses(CourseQueryParam courseQueryParam) {
        return null;
    }
}
