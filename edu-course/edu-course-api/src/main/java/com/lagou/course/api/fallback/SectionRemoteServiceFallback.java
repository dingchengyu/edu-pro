package com.lagou.course.api.fallback;

import com.lagou.course.api.SectionRemoteService;
import com.lagou.course.api.dto.SectionDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 11:49
 */
@Slf4j
public class SectionRemoteServiceFallback implements SectionRemoteService {
    @Override
    public boolean saveOrUpdateSection(SectionDTO sectionDTO) {
        log.info("SectionRemoteService.saveOrUpdateSection");
        return false;
    }

    @Override
    public List<SectionDTO> getSectionAndLesson(Integer courseId) {
        log.info("SectionRemoteService.getSectionAndLesson");
        return null;
    }

    @Override
    public SectionDTO getBySectionId(Integer sectionId) {
        log.info("SectionRemoteService.getBySectionId");
        return null;
    }
}
