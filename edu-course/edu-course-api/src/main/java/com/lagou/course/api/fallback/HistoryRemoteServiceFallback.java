package com.lagou.course.api.fallback;

import com.lagou.course.api.HistoryRemoteService;
import com.lagou.course.api.dto.PlayHistoryDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 11:43
 */

@Slf4j
public class HistoryRemoteServiceFallback implements HistoryRemoteService {
    @Override
    public void saveCourseHistoryNode(PlayHistoryDTO playHistoryDTO) {
        log.info("HistoryRemoteService.saveCourseHistoryNode ");
    }

    @Override
    public List hasStudyLessons(Integer userId, Integer courseId) {
        log.info("HistoryRemoteService.hasStudyLessons ");
        return null;
    }

    @Override
    public PlayHistoryDTO getByLessonId(Integer lessonId, Integer userId) {
        log.info("HistoryRemoteService.getByLessonId ");
        return null;
    }
}
