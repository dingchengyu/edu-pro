package com.lagou.course.api.fallback;

import com.lagou.course.api.MediaRemoteService;
import com.lagou.course.api.dto.MediaDTO;
import com.lagou.course.api.dto.VideoPlayDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/24 11:47
 */

@Slf4j
public class MediaRemoteServiceFallback implements MediaRemoteService {
    @Override
    public MediaDTO getByLessonId(Integer lessonId) {
        log.info("MediaRemoteService.getByLessonId");
        return null;
    }

    @Override
    public byte[] getCourseMediaDKByFileId(String fileId, String edk, Integer userId) {
        log.info("MediaRemoteService.getCourseMediaDKByFileId");
        return new byte[0];
    }

    @Override
    public void updateOrSaveMedia(MediaDTO cediaDTO) {
        log.info("MediaRemoteService.updateOrSaveMedia");
    }

    @Override
    public VideoPlayDto getVideoPlayInfo(Integer lessonId, Integer userId) {
        log.info("MediaRemoteService.getVideoPlayInfo");
        return null;
    }
}
