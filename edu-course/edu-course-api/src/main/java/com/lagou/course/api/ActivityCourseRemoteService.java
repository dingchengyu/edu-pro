package com.lagou.course.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.lagou.common.result.ResponseDTO;
import com.lagou.course.api.dto.ActivityCourseDTO;

/**
 * @author: dingchengyu
 * @date:   2020年7月6日 下午9:34:56
*/
@FeignClient(name = "edu-course", contextId = "activity", path = "/remote/activity")
public interface ActivityCourseRemoteService {
	
	@PostMapping("/saveActivityCourse")
	ResponseDTO<?> saveActivityCourse(@RequestBody ActivityCourseDTO reqDTO);
	
	@PostMapping("/updateActivityCourseStatus")
    ResponseDTO<?> updateActivityCourseStatus(@RequestBody ActivityCourseDTO reqDTO);
	
	@GetMapping("/getById")
    ResponseDTO<ActivityCourseDTO> getById(@RequestParam("id") Integer id);
	
	@GetMapping("/getByCourseId")
    ResponseDTO<ActivityCourseDTO> getByCourseId(@RequestParam("courseId") Integer courseId);
	
	@PostMapping("/updateActivityCourseStock")
	ResponseDTO<?> updateActivityCourseStock(@RequestParam("courseId") Integer courseId, @RequestParam("orderNo") String orderNo);
}
