package com.lagou.message.api;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.lagou.common.result.ResponseDTO;
import com.lagou.message.api.dto.MessageDTO;
import com.lagou.message.api.dto.MessageQueryDTO;

@FeignClient(name = "edu-message", path = "/message")
public interface MessageRemoteService {

	@PostMapping("/getMessageList")
	ResponseDTO<Page<MessageDTO>> getMessageList(@RequestBody MessageQueryDTO messageQueryDTO);
	
	@PostMapping("/updateReadStatus")
	ResponseDTO<Boolean> updateReadStatus(@RequestParam("userId") Integer userId);
	
	@GetMapping("/getUnReadMessageFlag")
	ResponseDTO<Boolean> getUnReadMessageFlag(@RequestParam("userId") Integer userId);

	@PostMapping("/putMessage")
	ResponseDTO<Boolean> putMessage(@RequestParam("lessonId") Integer lessonId);
}