package com.lagou.message.api.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LessonStatusReleaseDTO implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 4667691442836548033L;
	
	//课时id
	private Integer lessonId;
   
}
