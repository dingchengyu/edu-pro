package com.lagou.message.api.dto;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "通知消息查询对象")
public class MessageQueryDTO extends Page implements java.io.Serializable{
	
	/**
	 */
	private static final long serialVersionUID = 6459381920809382868L;
	
	private Integer userId;
}
