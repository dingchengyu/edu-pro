/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : edu_message

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 30/04/2022 16:50:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int(0) NOT NULL COMMENT '用户id',
  `course_id` int(0) NULL DEFAULT NULL COMMENT '课程id',
  `course_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课程名称',
  `course_lesson_id` int(0) NULL DEFAULT NULL COMMENT '课时id',
  `theme` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '课时主题',
  `has_read` tinyint(0) NULL DEFAULT 0 COMMENT '是否读取  0未读 1已读',
  `is_del` tinyint(0) NULL DEFAULT 0 COMMENT '是否删除 0未删除 1删除',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息通知表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (1, 100030017, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 14:19:31', '2022-04-30 14:19:31');
INSERT INTO `message` VALUES (2, 100030011, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 14:19:31', '2022-04-30 14:19:31');
INSERT INTO `message` VALUES (3, 100030019, 9, '秒杀112', 27, '215', 1, 0, NULL, '2022-04-30 14:19:31', '2022-04-30 14:19:31');
INSERT INTO `message` VALUES (4, 100030017, 9, '秒杀112', 14, '11111', 0, 0, NULL, '2022-04-30 14:22:01', '2022-04-30 14:22:01');
INSERT INTO `message` VALUES (5, 100030011, 9, '秒杀112', 14, '11111', 0, 0, NULL, '2022-04-30 14:22:01', '2022-04-30 14:22:01');
INSERT INTO `message` VALUES (6, 100030019, 9, '秒杀112', 14, '11111', 1, 0, NULL, '2022-04-30 14:22:01', '2022-04-30 14:22:01');
INSERT INTO `message` VALUES (7, 100030017, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 14:24:29', '2022-04-30 14:24:29');
INSERT INTO `message` VALUES (8, 100030011, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 14:24:29', '2022-04-30 14:24:29');
INSERT INTO `message` VALUES (9, 100030019, 9, '秒杀112', 27, '215', 1, 0, NULL, '2022-04-30 14:24:29', '2022-04-30 14:24:29');
INSERT INTO `message` VALUES (10, 100030018, 7, '文案高手的18项修炼', 11, '内容总结', 0, 0, NULL, '2022-04-30 14:27:53', '2022-04-30 14:27:53');
INSERT INTO `message` VALUES (11, 100030011, 7, '文案高手的18项修炼', 11, '内容总结', 0, 0, NULL, '2022-04-30 14:27:53', '2022-04-30 14:27:53');
INSERT INTO `message` VALUES (12, 100030017, 7, '文案高手的18项修炼', 11, '内容总结', 0, 0, NULL, '2022-04-30 14:27:53', '2022-04-30 14:27:53');
INSERT INTO `message` VALUES (13, 100030019, 7, '文案高手的18项修炼', 11, '内容总结', 1, 0, NULL, '2022-04-30 14:27:53', '2022-04-30 14:27:53');
INSERT INTO `message` VALUES (14, 100030021, 7, '文案高手的18项修炼', 11, '内容总结', 0, 0, NULL, '2022-04-30 14:27:53', '2022-04-30 14:27:53');
INSERT INTO `message` VALUES (15, 100030019, 7, '文案高手的18项修炼', 11, '内容总结', 1, 0, NULL, '2022-04-30 14:27:53', '2022-04-30 14:27:53');
INSERT INTO `message` VALUES (16, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 21, '组件学习', 0, 0, NULL, '2022-04-30 14:32:35', '2022-04-30 14:32:35');
INSERT INTO `message` VALUES (17, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 21, '组件学习', 0, 0, NULL, '2022-04-30 14:32:57', '2022-04-30 14:32:57');
INSERT INTO `message` VALUES (18, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 21, '组件学习', 0, 0, NULL, '2022-04-30 14:33:55', '2022-04-30 14:33:55');
INSERT INTO `message` VALUES (19, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 21, '组件学习', 0, 0, NULL, '2022-04-30 14:34:27', '2022-04-30 14:34:27');
INSERT INTO `message` VALUES (20, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 21, '组件学习', 0, 0, NULL, '2022-04-30 14:37:59', '2022-04-30 14:37:59');
INSERT INTO `message` VALUES (21, 100030018, 7, '文案高手的18项修炼', 9, '手把手教你写出爆款文案', 0, 0, NULL, '2022-04-30 15:52:27', '2022-04-30 15:52:27');
INSERT INTO `message` VALUES (22, 100030011, 7, '文案高手的18项修炼', 9, '手把手教你写出爆款文案', 0, 0, NULL, '2022-04-30 15:52:27', '2022-04-30 15:52:27');
INSERT INTO `message` VALUES (23, 100030017, 7, '文案高手的18项修炼', 9, '手把手教你写出爆款文案', 0, 0, NULL, '2022-04-30 15:52:27', '2022-04-30 15:52:27');
INSERT INTO `message` VALUES (24, 100030019, 7, '文案高手的18项修炼', 9, '手把手教你写出爆款文案', 1, 0, NULL, '2022-04-30 15:52:27', '2022-04-30 15:52:27');
INSERT INTO `message` VALUES (25, 100030021, 7, '文案高手的18项修炼', 9, '手把手教你写出爆款文案', 0, 0, NULL, '2022-04-30 15:52:27', '2022-04-30 15:52:27');
INSERT INTO `message` VALUES (26, 100030019, 7, '文案高手的18项修炼', 9, '手把手教你写出爆款文案', 1, 0, NULL, '2022-04-30 15:52:27', '2022-04-30 15:52:27');
INSERT INTO `message` VALUES (27, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 12, '开篇词 | 解析 Vue.js 源码，提升编码能力', 0, 0, NULL, '2022-04-30 15:53:50', '2022-04-30 15:53:50');
INSERT INTO `message` VALUES (28, 100030011, 8, 'Vue.js 3.0 核心源码解析123', 12, '开篇词 | 解析 Vue.js 源码，提升编码能力', 0, 0, NULL, '2022-04-30 15:53:58', '2022-04-30 15:53:58');
INSERT INTO `message` VALUES (29, 100030017, 9, '秒杀112', 28, '216', 0, 0, NULL, '2022-04-30 15:54:35', '2022-04-30 15:54:35');
INSERT INTO `message` VALUES (30, 100030011, 9, '秒杀112', 28, '216', 0, 0, NULL, '2022-04-30 15:54:35', '2022-04-30 15:54:35');
INSERT INTO `message` VALUES (31, 100030019, 9, '秒杀112', 28, '216', 1, 0, NULL, '2022-04-30 15:54:35', '2022-04-30 15:54:35');
INSERT INTO `message` VALUES (32, 100030017, 9, '秒杀112', 28, '216', 0, 0, NULL, '2022-04-30 15:54:41', '2022-04-30 15:54:41');
INSERT INTO `message` VALUES (33, 100030011, 9, '秒杀112', 28, '216', 0, 0, NULL, '2022-04-30 15:54:41', '2022-04-30 15:54:41');
INSERT INTO `message` VALUES (34, 100030019, 9, '秒杀112', 28, '216', 1, 0, NULL, '2022-04-30 15:54:41', '2022-04-30 15:54:41');
INSERT INTO `message` VALUES (35, 100030017, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 15:58:21', '2022-04-30 15:58:21');
INSERT INTO `message` VALUES (36, 100030011, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 15:58:21', '2022-04-30 15:58:21');
INSERT INTO `message` VALUES (37, 100030019, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 15:58:21', '2022-04-30 15:58:21');
INSERT INTO `message` VALUES (38, 100030017, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 16:13:38', '2022-04-30 16:13:38');
INSERT INTO `message` VALUES (39, 100030011, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 16:13:38', '2022-04-30 16:13:38');
INSERT INTO `message` VALUES (40, 100030019, 9, '秒杀112', 27, '215', 0, 0, NULL, '2022-04-30 16:13:38', '2022-04-30 16:13:38');

SET FOREIGN_KEY_CHECKS = 1;
