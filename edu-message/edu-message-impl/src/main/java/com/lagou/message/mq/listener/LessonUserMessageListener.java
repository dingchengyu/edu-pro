package com.lagou.message.mq.listener;

import com.alibaba.fastjson.JSON;
import com.lagou.common.constant.MQConstant;
import com.lagou.common.mq.dto.BaseMqDTO;
import com.lagou.common.util.ValidateUtils;
import com.lagou.message.api.dto.LessonStatusReleaseDTO;
import com.lagou.message.api.dto.Message;
import com.lagou.message.service.IMessageService;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
@RocketMQMessageListener(topic = MQConstant.Topic.LESSON_RELEASE_SEND_MESSAGE, consumerGroup = MQConstant.Topic.MESSAGE_CONSUMER_USER)
public class LessonUserMessageListener implements RocketMQListener<BaseMqDTO<List<Integer>>>{

    @Autowired
    private IMessageService messageService;

    @Override
    public void onMessage(BaseMqDTO<List<Integer>> data) {
        log.info("LessonUserMessageListener  onMessage - data:{} " + JSON.toJSONString(data));
        ValidateUtils.notNullParam(data);
        List<Integer> userList = data.getData();
        if(CollectionUtils.isEmpty(userList)){
            return;
        }
        Message message = null;
        for(Integer userId : userList){
            message = new Message();
            message.setUserId(userId);
            messageService.sendMessage(message);
        }
    }
}