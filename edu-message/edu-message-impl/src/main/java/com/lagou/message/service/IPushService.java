package com.lagou.message.service;


import com.lagou.message.entity.Message;

public interface IPushService {

    /**
     * 向页面推送消息
     * @param message
     */
    void push(Message message);
}
