package com.lagou.message.mapper;

import com.lagou.message.entity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息通知表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-06
 */
public interface MessageMapper extends BaseMapper<Message> {

}
