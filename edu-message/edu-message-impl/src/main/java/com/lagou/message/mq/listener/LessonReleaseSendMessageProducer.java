package com.lagou.message.mq.listener;

import java.util.List;
import java.util.UUID;

import com.lagou.common.mq.dto.BaseMqDTO;
import com.lagou.message.api.dto.LessonStatusReleaseDTO;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import com.lagou.common.constant.MQConstant;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class LessonReleaseSendMessageProducer {

    @Autowired
    RocketMQTemplate rocketMQTemplate;

    public void sendMessage(LessonStatusReleaseDTO data){
        Message<LessonStatusReleaseDTO> message = MessageBuilder.withPayload(data).build();
        rocketMQTemplate.asyncSend(MQConstant.Topic.LESSON_STATUS_RELEASE, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("消息发生成功！");
            }

            @Override
            public void onException(Throwable e) {
                log.info("消息发送失败！error e:",e);
            }
        });
    }

    public void sendMessageToUser(List<Integer> userIdList){
        Message<List<Integer>> message = MessageBuilder.withPayload(userIdList).build();

        rocketMQTemplate.asyncSend(MQConstant.Topic.LESSON_RELEASE_SEND_MESSAGE, new BaseMqDTO<>(userIdList, UUID.randomUUID().toString()), new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("用户消息发生成功！");
            }

            @Override
            public void onException(Throwable e) {
                log.info("用户消息发送失败！error e:",e);
            }
        });
    }

}