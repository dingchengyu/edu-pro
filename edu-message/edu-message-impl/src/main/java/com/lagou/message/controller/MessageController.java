package com.lagou.message.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.common.result.ResponseDTO;
import com.lagou.message.api.dto.LessonStatusReleaseDTO;
import com.lagou.message.api.dto.MessageDTO;
import com.lagou.message.api.dto.MessageQueryDTO;
import com.lagou.message.mq.listener.LessonReleaseSendMessageProducer;
import com.lagou.message.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 消息通知表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-06
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    @Autowired
    private IMessageService messageService;
    @Autowired
    private LessonReleaseSendMessageProducer producer;

    @PostMapping("/getMessageList")
    public ResponseDTO<Page<MessageDTO>> getCourseOrderByOrderNo(@RequestBody MessageQueryDTO messageQueryDTO) {
        return ResponseDTO.success(messageService.getMessageByUserId(messageQueryDTO));
    }

    @PostMapping("/updateReadStatus")
    public ResponseDTO<Boolean> updateReadStatus(@RequestParam("userId") Integer userId){
        return ResponseDTO.success(messageService.updateReadStatus(userId));
    }

    @GetMapping("/getUnReadMessageFlag")
    public ResponseDTO<Boolean> getUnReadMessageFlag(@RequestParam("userId") Integer userId){
        return ResponseDTO.success(messageService.getUnReadMessageFlag(userId));
    }

    @PostMapping("/putMessage")
    public ResponseDTO<Boolean> putMessage(@RequestParam("lessonId") Integer lessonId){
        LessonStatusReleaseDTO lessonStatusRelease = new LessonStatusReleaseDTO();
        lessonStatusRelease.setLessonId(lessonId);
        producer.sendMessage(lessonStatusRelease);
        return ResponseDTO.success();
    }
}
