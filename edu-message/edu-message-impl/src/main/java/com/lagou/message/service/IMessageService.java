package com.lagou.message.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lagou.message.api.dto.MessageDTO;
import com.lagou.message.api.dto.MessageQueryDTO;
import com.lagou.message.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 消息通知表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-06
 */
public interface IMessageService extends IService<Message> {

    Page<MessageDTO> getMessageByUserId(MessageQueryDTO messageQueryDTO);

    Boolean updateReadStatus(Integer userId);

    List<Integer> saveMessage(Integer lessonId);

    void sendMessage(com.lagou.message.api.dto.Message message);

    Boolean getUnReadMessageFlag(Integer userId);
}
