package com.lagou.message.mq.listener;

import com.lagou.common.mq.dto.BaseMqDTO;
import io.jsonwebtoken.lang.Collections;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.lagou.common.constant.MQConstant;
import com.lagou.message.api.dto.LessonStatusReleaseDTO;
import com.lagou.message.service.IMessageService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

@Slf4j
@Component
@RocketMQMessageListener(topic = MQConstant.Topic.LESSON_STATUS_RELEASE, consumerGroup = MQConstant.Topic.MESSAGE_CONSUMER)
public class LessonStatusReleaseListener implements RocketMQListener<LessonStatusReleaseDTO>{

    @Autowired
    private  IMessageService messageService;
    @Autowired
    private LessonReleaseSendMessageProducer lessonReleaseSendMessageProducer;

    @Override
    public void onMessage(LessonStatusReleaseDTO message) {
        log.info("收到消息： " + JSON.toJSONString(message));
        List<Integer> userIdList = messageService.saveMessage(message.getLessonId());
        //用户不等于空，给用户推送消息
        if(!Collections.isEmpty(userIdList)){
            lessonReleaseSendMessageProducer.sendMessageToUser(userIdList);
        }
    }
}