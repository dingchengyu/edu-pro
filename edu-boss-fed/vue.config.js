/**
 * Override convention configuration
 * https://cli.vuejs.org/config/
 */

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/edu-boss-fed/' : '/',
  indexPath: 'template/index.html',
  assetsDir: 'assets',
  lintOnSave: process.env.NODE_ENV !== 'production',
  productionSourceMap: false,
  devServer: {
    proxy: {
      '^/boss': {
        target: 'http://192.168.1.4:8010',
        pathRewrite: { '^/boss': '/' },
        changeOrigin: true
      },
      '^/front': {
        target: 'http://192.168.1.4:8081',
        pathRewrite: { '^/front': '/' },
        changeOrigin: true
      },
      '^/user': {
        target: 'http://192.168.1.4:2223',
        changeOrigin: true,
        secure: false
      },
      '^/course': {
        target: 'http://192.168.1.4:8081',
        changeOrigin: true,
        secure: false
      }
    }
  }
}
