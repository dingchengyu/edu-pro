package com.lagou.boss.service;

import com.lagou.boss.entity.form.CourseForm;
import com.lagou.boss.entity.vo.CourseVo;
import com.lagou.course.api.dto.CourseDTO;
import com.lagou.course.api.dto.PageResultDTO;
import com.lagou.course.api.param.CourseQueryParam;
import org.jsoup.nodes.Element;
import org.springframework.web.bind.annotation.RequestParam;

public interface ICourseService {

    void removeImgWidthAttribute(Element element);

    /**
     * 保存或更新课程
     * @param courseForm
     * @return
     */
    CourseDTO saveOrUpdateCourse(CourseForm courseForm);

    /**
     * 获取课程信息
     * @param courseId
     * @return
     */
    CourseVo getCourseById(Integer courseId);

    /**
     * 分页查询数据
     * @param courseQueryParam
     * @return
     */
    PageResultDTO<CourseDTO> getQueryCourses(CourseQueryParam courseQueryParam);

    Boolean changeState(Integer courseId, Integer status);


}
