package com.lagou.boss.service.impl;

import com.lagou.boss.entity.form.CourseForm;
import com.lagou.boss.entity.vo.CourseVo;
import com.lagou.boss.service.ICourseService;
import com.lagou.course.api.CourseRemoteService;
import com.lagou.course.api.dto.CourseDTO;
import com.lagou.course.api.dto.PageResultDTO;
import com.lagou.course.api.param.CourseQueryParam;
import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author:   dingchengyu
 * Date:     2021/3/28
 * Description: 课程
 */
@Service
public class CourseService implements ICourseService {

    @Autowired
    private CourseRemoteService courseRemoteService;

    public final static String host_prefix = "http://www.lgstatic.com/";
    public final static String https_host_prefix = "https://www.lgstatic.com/";

    @Override
    public void removeImgWidthAttribute(Element element) {
        if (element.tagName().equals("img")) {

            String attr = element.attr("src");
            if (StringUtils.isNotEmpty(attr) && !attr.startsWith(host_prefix) && !attr.startsWith(https_host_prefix)) {
                throw new RuntimeException("图片未完全保存成功！");
            }

            element.attr("width", "100%");
            element.attr("height", "100%");
        }
        Elements childs = element.select("> *");
        if (childs != null && !childs.isEmpty()) {
            for (Element child : childs) {
                removeImgWidthAttribute(child);
            }
        }
    }

    private CourseDTO copDataToDTO(CourseForm courseForm) {
        //缺少 courseDescription totalCourseTime    actualSales(真是销量通过订单状态变更)  courseImgUrl
        CourseDTO courseDTO = new CourseDTO();
        courseDTO.setId(courseForm.getId());
        courseDTO.setCourseName(courseForm.getCourseName());
        courseDTO.setBrief(courseForm.getBrief());
        courseDTO.setPrice(courseForm.getPrice());
        courseDTO.setPriceTag(courseForm.getPriceTag());
        courseDTO.setDiscounts(courseForm.getDiscounts());
        courseDTO.setDiscountsTag(courseForm.getDiscountsTag());
        courseDTO.setCourseDescriptionMarkDown(courseForm.getCourseDescriptionMarkDown());
        courseDTO.setCourseImgUrl(courseForm.getCourseImgUrl());
        courseDTO.setIsNew(courseForm.getIsNew());
        courseDTO.setAutoOnlineTime(courseForm.getAutoOnlineTime());
        courseDTO.setCourseListImg(courseForm.getCourseListImg());
        courseDTO.setCourseUrl(courseForm.getCourseImgUrl());
        courseDTO.setIsNewDes(courseForm.getIsNewDes());
        courseDTO.setIsNewDes(courseForm.getIsNewDes());
        courseDTO.setAutoOnlineTime(courseForm.getAutoOnlineTime());
        courseDTO.setIsDel(Boolean.FALSE);
        courseDTO.setStatus(courseForm.getStatus());
        courseDTO.setSortNum(courseForm.getSortNum());
        courseDTO.setTeacherId(courseForm.getTeacherDTO().getId());
        courseDTO.setPreviewFirstField(courseForm.getPreviewFirstField());
        courseDTO.setPreviewSecondField(courseForm.getPreviewSecondField());
        courseDTO.setSales(courseForm.getSales());
        courseDTO.setTeacherDTO(courseForm.getTeacherDTO());
        return courseDTO;
    }

    @Override
    public CourseDTO saveOrUpdateCourse(CourseForm courseForm) {
        CourseDTO courseDTO = copDataToDTO(courseForm);
        courseRemoteService.saveOrUpdateCourse(courseDTO);
        return courseDTO;
    }

    @Override
    public CourseVo getCourseById(Integer courseId) {
        CourseDTO courseDTO =  courseRemoteService.getCourseById(courseId);
        CourseVo courseVo = new CourseVo();
        BeanUtils.copyProperties(courseDTO,courseVo);
        return courseVo;
    }

    @Override
    public PageResultDTO<CourseDTO> getQueryCourses(CourseQueryParam courseQueryParam) {
        return courseRemoteService.getQueryCourses(courseQueryParam);
    }

    @Override
    public Boolean changeState(Integer courseId, Integer status) {
        CourseDTO courseDTO =  courseRemoteService.getCourseById(courseId);
        courseDTO.setStatus(status);
        return courseRemoteService.saveOrUpdateCourse(courseDTO);
    }
}
