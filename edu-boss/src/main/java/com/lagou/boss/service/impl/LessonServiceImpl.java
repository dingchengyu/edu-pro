package com.lagou.boss.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lagou.boss.service.ILessonService;
import com.lagou.common.result.ResultCode;
import com.lagou.common.util.ValidateUtils;
import com.lagou.course.api.LessonRemoteService;
import com.lagou.course.api.dto.LessonDTO;
import com.lagou.course.api.enums.CourseLessonStatus;


@Service
public class LessonServiceImpl implements ILessonService {

	@Autowired
    private LessonRemoteService lessonRemoteService;

	@Override
	public boolean saveOrUpdate(LessonDTO lessonDTO) {
		if(lessonDTO.getId() != null && lessonDTO.getStatus() != null && lessonDTO.getStatus().equals(CourseLessonStatus.RELEASE.getCode())) {
			//查看是否是上架操作
			LessonDTO lessonDB = lessonRemoteService.getById(lessonDTO.getId());
			ValidateUtils.notNull(lessonDB, ResultCode.ALERT_ERROR.getState(), "课时信息查询为空");
		}
		return lessonRemoteService.saveOrUpdate(lessonDTO);
	}
}
