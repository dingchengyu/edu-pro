package com.lagou.pay.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.common.result.ResponseDTO;
import com.lagou.common.util.ConvertUtils;
import com.lagou.pay.api.dto.*;
import com.lagou.pay.entity.PayOrder;
import com.lagou.pay.service.IPayOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 支付控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-26
 */
@Slf4j
@RestController
@RequestMapping("/pay/order")
public class OrderController {

    @Autowired
    private IPayOrderService payOrderService;

    /**
     * 查询订单信息
     * @param userId
     * @param orderNo
     * @return
     */
    @GetMapping("/getOrderByNoAndUserId")
    public ResponseDTO<PayOrderDTO> getOrderByNo(@RequestParam("userId") Integer userId, @RequestParam("orderNo") String orderNo) {
        PayOrder orderDB = payOrderService.getOne(new QueryWrapper<PayOrder>()
                .eq("order_no", orderNo).eq("user_id", userId));
        if(null == orderDB) {
            return ResponseDTO.ofError("订单信息查询为空");
        }
        return ResponseDTO.success(ConvertUtils.convert(orderDB, PayOrderDTO.class));
    }

    /**
     * 创建订单
     * @param reqDTO
     * @return
     */
    @PostMapping("/saveOrder")
    public ResponseDTO<PayResDTO> saveOrder(@RequestBody PayReqDTO reqDTO) {
        log.info("saveOrder - reqDTO:{}", JSON.toJSONString(reqDTO));
        return ResponseDTO.success(payOrderService.saveOrder(reqDTO));
    }

    /**
     * 支付回调
     * @param request
     * @return
     */
    @PostMapping("/payCallBack")
    public ResponseDTO<CallBackRes> payCallBack(@RequestBody CallBackReq request){
        log.info("callBack - request:{}",JSON.toJSONString(request));
        return ResponseDTO.success(payOrderService.callBack(request));
    }
}
