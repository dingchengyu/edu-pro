package com.lagou.pay;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/3/1 22:44
 */
@EnableConfigurationProperties
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.lagou")
@MapperScan("com.lagou.pay.mapper")
public class PayApplication {
    public static void main(String[] args) {
        SpringApplication.run(PayApplication.class, args);
    }
    
}
