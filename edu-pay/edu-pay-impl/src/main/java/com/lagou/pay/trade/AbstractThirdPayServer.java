package com.lagou.pay.trade;

import com.lagou.pay.trade.request.BasePayRequest;
import com.lagou.pay.trade.response.BasePayResponse;

/**
 * @Description:(三方服务抽象服务类)   
 * @author: dingchengyu
 * @date:   2020年6月19日 下午1:37:45
*/
public abstract class AbstractThirdPayServer<R extends BasePayRequest,P extends BasePayResponse> implements ThirdPayServer<R,P>{

	public final static String PAY_SERVER = "_pay_server";
}
