package com.lagou.pay.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 支付订单状态日志表
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PayOrderRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 操作类型：CREATE|PAY|REFUND...
     */
    private String type;

    /**
     * 原订单状态
     */
    private String fromStatus;

    /**
     * 新订单状态
     */
    private String toStatus;

    /**
     * 实付金额，单位为分
     */
    private Integer paidAmount;

    /**
     * 备注
     */
    private String remark;

    /**
     * 操作人
     */
    private String createBy;

    /**
     * 操作时间
     */
    private Date createTime;


}
