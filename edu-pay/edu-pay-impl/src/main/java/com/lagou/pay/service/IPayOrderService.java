package com.lagou.pay.service;

import com.lagou.course.api.dto.CourseDTO;
import com.lagou.pay.api.dto.CallBackReq;
import com.lagou.pay.api.dto.CallBackRes;
import com.lagou.pay.api.dto.PayReqDTO;
import com.lagou.pay.api.dto.PayResDTO;
import com.lagou.pay.entity.PayOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lagou.pay.mq.dto.CancelPayOrderDTO;

import java.math.BigDecimal;

/**
 * <p>
 * 支付订单信息表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-02
 */
public interface IPayOrderService extends IService<PayOrder> {

    PayResDTO saveOrder(PayReqDTO reqDTO);

    CallBackRes callBack(CallBackReq request);

    PayOrder getLastOrder(Integer userId,Integer courseId,String channel);

    PayOrder saveOrder(PayReqDTO reqDTO, CourseDTO courseDTO, BigDecimal activityAmount);

    void cancelPayOrder(CancelPayOrderDTO cancelPayOrderDTO);

    boolean updateStatusInvalid(PayOrder payOrder);
}
