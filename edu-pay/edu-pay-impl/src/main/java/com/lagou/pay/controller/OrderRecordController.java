package com.lagou.pay.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 支付订单状态日志表 前端控制器
 * </p>
 *
 * @author dingchengyu
 * @since 2022-02-26
 */
@RestController
@RequestMapping("/pay/order-record")
public class OrderRecordController {

}
