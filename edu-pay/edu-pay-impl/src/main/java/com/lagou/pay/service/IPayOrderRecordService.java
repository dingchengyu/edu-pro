package com.lagou.pay.service;

import com.lagou.pay.entity.PayOrderRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 支付订单状态日志表 服务类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-02
 */
public interface IPayOrderRecordService extends IService<PayOrderRecord> {

}
