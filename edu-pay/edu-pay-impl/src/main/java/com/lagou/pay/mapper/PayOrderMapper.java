package com.lagou.pay.mapper;

import com.lagou.pay.entity.PayOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付订单信息表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-02
 */
public interface PayOrderMapper extends BaseMapper<PayOrder> {

}
