package com.lagou.pay.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.common.result.ResponseDTO;
import com.lagou.common.result.ResultCode;
import com.lagou.common.util.IDUtil;
import com.lagou.common.util.ValidateUtils;
import com.lagou.course.api.ActivityCourseRemoteService;
import com.lagou.course.api.CourseRemoteService;
import com.lagou.course.api.dto.ActivityCourseDTO;
import com.lagou.course.api.dto.CourseDTO;
import com.lagou.order.api.UserCourseOrderRemoteService;
import com.lagou.order.api.dto.UserCourseOrderDTO;
import com.lagou.order.api.enums.StatusTypeEnum;
import com.lagou.order.api.enums.UserCourseOrderStatus;
import com.lagou.pay.annotation.PayOrderRecord;
import com.lagou.pay.api.dto.CallBackReq;
import com.lagou.pay.api.dto.CallBackRes;
import com.lagou.pay.api.dto.PayReqDTO;
import com.lagou.pay.api.dto.PayResDTO;
import com.lagou.pay.api.enums.Channel;
import com.lagou.pay.api.enums.Currency;
import com.lagou.pay.api.enums.OrderType;
import com.lagou.pay.api.enums.Status;
import com.lagou.pay.entity.PayOrder;
import com.lagou.pay.mapper.PayOrderMapper;
import com.lagou.pay.model.OutTrade;
import com.lagou.pay.mq.dto.CancelPayOrderDTO;
import com.lagou.pay.service.IPayOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lagou.pay.trade.ThirdPayService;
import com.lagou.pay.trade.request.BasePayRequest;
import com.lagou.pay.trade.request.aliPay.AliPayRequest;
import com.lagou.pay.trade.request.wechatPay.WechatPayRequest;
import com.lagou.pay.trade.response.BasePayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 支付订单信息表 服务实现类
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-02
 */

@Slf4j
@Service
public class PayOrderServiceImpl extends ServiceImpl<PayOrderMapper, PayOrder> implements IPayOrderService {

    @Autowired
    private CourseRemoteService courseRemoteService;
    @Autowired
    private UserCourseOrderRemoteService userCourseOrderRemoteService;
    @Autowired
    private ThirdPayService thirdPayService;
    //    @Autowired
//    private RocketMqService rocketMqService;
    @Autowired
    private ActivityCourseRemoteService activityCourseRemoteService;

    @Override
    public PayResDTO saveOrder(PayReqDTO reqDTO) {
        log.info("saveOrder - reqDTO:{}", JSON.toJSONString(reqDTO));

        ResponseDTO<UserCourseOrderDTO> resp = userCourseOrderRemoteService.getCourseOrderByOrderNo(reqDTO.getGoodsOrderNo());
        log.info("saveOrder - userCourseOrderRemoteService.getCourseOrderByOrderNo - GoodsOrderNo:{} resp:{}",reqDTO.getGoodsOrderNo(),JSON.toJSONString(resp));
        ValidateUtils.isTrue(resp.isSuccess(), resp.getState(),resp.getMessage());

        CourseDTO courseDTO = courseRemoteService.getUserCourseById(resp.getContent().getCourseId(),reqDTO.getUserid());
        log.info("saveOrder - courseRemoteService.getCourseById - CourseId:{} courseDTO:{}",resp.getContent().getCourseId(),JSON.toJSONString(courseDTO));
        ValidateUtils.isTrue(null != courseDTO, "课程信息查询为空");

        PayResDTO res = new PayResDTO();
        //校验是否购买成功过
        PayOrder orderDB = checkSuccessBuyGoods(resp.getContent().getCourseId(), reqDTO.getUserid());
        if (null != orderDB) {
            res.setOrderNo(orderDB.getOrderNo());
            res.setStatus(Status.PAY_SUCCESS.getCode());
            return res;
        }
        //查询最新的记录
        PayOrder lastPayOrder = getLastOrder(reqDTO.getUserid(), resp.getContent().getCourseId(),reqDTO.getChannel());
        if(null == lastPayOrder || !lastPayOrder.getStatus().equals(Status.NOT_PAY.getCode())) {
            BigDecimal activityAmount = null;
            if(resp.getContent().getActivityCourseId() > 0) {
                //查询课程活动金额
                ResponseDTO<ActivityCourseDTO> respAC = activityCourseRemoteService.getById(resp.getContent().getActivityCourseId());
                ValidateUtils.isTrue(respAC.isSuccess(), respAC.getMessage());
                activityAmount = new BigDecimal(respAC.getContent().getAmount());
            }
            //创建订单
            lastPayOrder = saveOrder(reqDTO, courseDTO,activityAmount);
        }

        BasePayRequest request = new BasePayRequest();
        request.setOrder(lastPayOrder);
        request.setChannel(reqDTO.getChannel());
        request.setSource(reqDTO.getSource());
        //调用三方服务创建订单
        BasePayResponse payRes = thirdPayService.submitPay(request);
        log.info("saveOrder - thirdPayService.submitPay - request:{} payRes:{}",JSON.toJSONString(request),JSON.toJSONString(payRes));
        ValidateUtils.isTrue(payRes.isSuccess(), ResultCode.ALERT_ERROR.getState(),"提交三方支付服务异常");
        //延时消息 取消支付订单  TODO记得改下 延时级别
        // TODO 消息队列待完善
//        rocketMqService.sendDelayed(MQConstant.Topic.CANCEL_PAY_ORDER, new BaseMqDTO<CancelPayOrderDTO>(new CancelPayOrderDTO(lastPayOrder.getId()),UUID.randomUUID().toString()), MQConstant.DelayLevel.level_18);
        res.setChannel(reqDTO.getChannel());
        res.setOrderNo(lastPayOrder.getOrderNo());
        res.setPayUrl(payRes.getUrl());
        res.setSource(reqDTO.getSource().getCode());
        res.setStatus(lastPayOrder.getStatus());
        return res;
    }

    @Override
    public CallBackRes callBack(CallBackReq request) {
        String channelName = request.getChannel();
        BasePayResponse response = null;
        switch (Channel.ofName(channelName)) {
            case WECHAT:
                WechatPayRequest wechatPayRequest = new WechatPayRequest();
                wechatPayRequest.setParamStr(request.getWxCallBackReqStr());
                wechatPayRequest.setChannel(channelName);
                response = thirdPayService.callBack(wechatPayRequest);
                break;
            case ALIPAY:
                AliPayRequest aliPayRequest = new AliPayRequest();
                aliPayRequest.setChannel(channelName);
                aliPayRequest.setParams(request.getAliParams());
                response = thirdPayService.callBack(aliPayRequest);
                break;
            default:
                ValidateUtils.isTrue(false, "支付渠道错误");
        }

        OutTrade trade = response.getTrade();

        PayOrder payOrderDB = getOne(new QueryWrapper<PayOrder>().eq("order_no", trade.getOrderNo()));
        ValidateUtils.isTrue(null != payOrderDB, "支付订单信息查询为空");
        //更新商品订单
        if(Status.parse(trade.getStatus().getCode()) == Status.PAY_SUCCESS) {
            ResponseDTO<?> resp = userCourseOrderRemoteService.updateOrderStatus(payOrderDB.getGoodsOrderNo(), UserCourseOrderStatus.SUCCESS.getCode());
            log.info("callBack - userCourseOrderRemoteService.updateOrderStatus - GoodsOrderNo:{} resp:{}",payOrderDB.getGoodsOrderNo(),JSON.toJSONString(resp));
            ValidateUtils.isTrue(resp.isSuccess(), resp.getState(),resp.getMessage());
        }
        //更新支付订单信息
        PayOrder updatePayOrder = new PayOrder();
        updatePayOrder.setId(payOrderDB.getId());
        updatePayOrder.setOutTradeNo(trade.getOutTradeNo());
        updatePayOrder.setStatus(trade.getStatus().getCode());
        updatePayOrder.setAmount(payOrderDB.getAmount());
        updatePayOrder.setOrderNo(payOrderDB.getOrderNo());
        if(trade.getStatus().equals(Status.PAY_SUCCESS)) {
            updatePayOrder.setPayTime(trade.getPayTime());
        }
        ValidateUtils.isTrue(updateById(updatePayOrder), "支付订单更新失败");
        CallBackRes res = new CallBackRes();
        res.setResStr(response.getResStr());
        return res;
    }

    @Override
    public PayOrder getLastOrder(Integer userId, Integer courseId, String channel) {
        List<PayOrder> payOrderList = list(new QueryWrapper<PayOrder>()
                .eq("product_id", courseId)
                .eq("user_id", userId)
                .eq("channel", channel)
                .orderByDesc("id"));
        if(null != payOrderList && payOrderList.size() > 0) {
            return payOrderList.get(0);
        }
        return null;
    }

    @Override
    @PayOrderRecord(type = StatusTypeEnum.INSERT)
    public PayOrder saveOrder(PayReqDTO reqDTO, CourseDTO courseDTO, BigDecimal activityAmount) {
        PayOrder order = new PayOrder();
        order.setOrderNo(IDUtil.getRandomId().toString());
        //TODO 暂时写死1分钱
        order.setAmount(new BigDecimal(0.01));
//        if(null != activityAmount) {
//        	order.setAmount(activityAmount);
//        }else {
//          order.setAmount((null != courseDTO.getDiscounts() && courseDTO.getDiscounts() > 0) ? new BigDecimal(courseDTO.getDiscounts()) : new BigDecimal(courseDTO.getPrice()));
//        }
        order.setCount(1);
        order.setOrderType(OrderType.BUY_COURSE.getCode());
        order.setChannel(reqDTO.getChannel());
        order.setProductId(courseDTO.getId());
        order.setCurrency(Currency.GBEANS.name());
        order.setSource(reqDTO.getSource().getCode());
        order.setProductName(courseDTO.getCourseName());
        order.setStatus(Status.NOT_PAY.getCode());
        order.setGoodsOrderNo(reqDTO.getGoodsOrderNo());
        order.setCreateTime(new Date());
        order.setUpdateTime(order.getCreateTime());
        order.setUserId(reqDTO.getUserid());
        order.setClientIp(reqDTO.getClientIp());
        save(order);
        return order;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void cancelPayOrder(CancelPayOrderDTO cancelPayOrderDTO) {
        PayOrder payOrder = this.getById(cancelPayOrderDTO.getOrderId());
        ValidateUtils.notNull(payOrder, ResultCode.ALERT_ERROR.getState(), "查询支付订单信息为空:orderId:" + cancelPayOrderDTO.getOrderId());

        if(!payOrder.getStatus().equals(Status.NOT_PAY.getCode())){
            //支付订单已经终态 直接返回
            log.warn("支付订单已经终态  payOrderDB:{}", JSON.toJSONString(payOrder));
            return;
        }
        ValidateUtils.isTrue(updateStatusInvalid(payOrder), "支付订单更新为失效失败");
        ResponseDTO<?> resp = userCourseOrderRemoteService.updateOrderStatus(payOrder.getGoodsOrderNo(), UserCourseOrderStatus.CANCEL.getCode());
        ValidateUtils.isTrue(resp.isSuccess(), resp.getState(),resp.getMessage());
        //如果是活动商品还原库存
        ResponseDTO<?> respStock = activityCourseRemoteService.updateActivityCourseStock(payOrder.getProductId(),payOrder.getGoodsOrderNo());
        ValidateUtils.isTrue(respStock.isSuccess(), respStock.getState(),respStock.getMessage());
    }

    @Override
    @PayOrderRecord(type = StatusTypeEnum.CANCEL)
    @Transactional(rollbackFor = Exception.class)
    public boolean updateStatusInvalid(PayOrder payOrder) {
        PayOrder updatePayOrder = new PayOrder();
        updatePayOrder.setStatus(Status.INVALID.getCode());
        boolean res = this.update(updatePayOrder, new QueryWrapper<PayOrder>()
                .eq("id", payOrder.getId())
                .eq("status", Status.NOT_PAY.getCode()));
        return res;
    }

    /**
     * @Description: (检查是否购买成功过该商品)
     * @date:   2020年6月18日 上午11:23:27
     */
    PayOrder checkSuccessBuyGoods(Integer goodId,Integer userId) {
        return getOne(new QueryWrapper<PayOrder>()
                .eq("product_id", goodId)
                .eq("user_id", userId)
                .eq("status", Status.PAY_SUCCESS));
    }
}
