package com.lagou.pay.mapper;

import com.lagou.pay.entity.PayOrderRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付订单状态日志表 Mapper 接口
 * </p>
 *
 * @author dingchengyu
 * @since 2022-03-02
 */
public interface PayOrderRecordMapper extends BaseMapper<PayOrderRecord> {

}
