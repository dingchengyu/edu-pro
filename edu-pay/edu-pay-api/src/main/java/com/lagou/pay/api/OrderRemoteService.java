package com.lagou.pay.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.lagou.common.result.ResponseDTO;
import com.lagou.pay.api.dto.CallBackReq;
import com.lagou.pay.api.dto.CallBackRes;
import com.lagou.pay.api.dto.PayOrderDTO;
import com.lagou.pay.api.dto.PayReqDTO;
import com.lagou.pay.api.dto.PayResDTO;

/**
 * @author: dingchengyu
 * 2020年6月17日 上午11:40:59
 */
@FeignClient(name = "edu-pay", path = "/pay/order")
public interface OrderRemoteService {

	/**
	 * @Description: (查询订单信息)   
	*/
    @GetMapping("/getOrderByNoAndUserId")
    ResponseDTO<PayOrderDTO> getOrderByNoAndUserId(@RequestParam("userId") Integer userId, @RequestParam("orderNo") String orderNo);
    
    /**
     * @Description: (保存订单信息)   
    */
    @PostMapping("/saveOrder")
    ResponseDTO<PayResDTO> saveOrder(@RequestBody PayReqDTO reqDTO);
    
    /**
     * @Description: (支付结果回调通知)   
    */
    @PostMapping("/payCallBack")
    ResponseDTO<CallBackRes> payCallBack(@RequestBody CallBackReq request);
}