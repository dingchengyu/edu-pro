package com.lagou.pay.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * 类注释
 *
 * @author dingchengyu
 * @date 2022/2/26 17:26
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CallBackReq implements Serializable {

    private String channel;//支付渠道

    private String wxCallBackReqStr;//微信回调请求参数

    private Map<String, String> aliParams;//支付宝回调请求参数
}
