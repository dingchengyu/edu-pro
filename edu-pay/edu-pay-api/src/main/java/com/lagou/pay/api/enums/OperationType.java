package com.lagou.pay.api.enums;

/**
 * @author: dingchengyu
 * @date:   2020年6月22日 下午2:01:57
 */
public enum OperationType{
	CREATE,//创建
	PAY;//支付
}
