package com.lagou.oauth.dao;

import com.lagou.oauth.pojo.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users,Long> {

    Users findByUsername(String name);
}
